{
    "id": "e5b2ac3e-309e-43ab-b1de-1916087b1908",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fn_default",
    "AntiAlias": 0,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Papyrus",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "74acf9b4-574d-431e-b7c6-65d462e74a45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 136,
                "y": 71
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2587f2fd-4b2f-4f30-bdff-a3bfd4e8fac0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 81,
                "y": 71
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c56fe3df-fe0c-4ab2-8bb5-0c58a728d010",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 64,
                "y": 71
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6ac4bd7f-6513-4e84-be8e-d862b7b96f29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 29,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b83f39a5-772a-4696-b7c4-907bd07c8804",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 137,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "3597a6d5-af22-448d-a4a6-7deccbf753b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 208,
                "y": 25
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "06034f36-e4b1-4d49-bca4-05ad1b01f551",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 136,
                "y": 25
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "dbf4a19c-e65c-4375-89c4-7549b454ab8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 141,
                "y": 71
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "5d849e72-934a-4685-b23d-f0cdf1988f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 37,
                "y": 71
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "576803a6-34db-4d2b-8347-3b1e5f9ea796",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 70,
                "y": 71
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1981f356-06ed-4974-a5d4-8c03de1c7974",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 58,
                "y": 71
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "d5408f11-6e1c-4f36-ae0a-89c7844cdbdf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 201,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "f0e88dae-766b-4060-8182-c2e5397487b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 106,
                "y": 71
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "23c6fb83-4eac-4635-b134-2b74247f6bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 44,
                "y": 71
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "e764d6df-8214-46e5-a289-e4ec2dc50b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 111,
                "y": 71
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "cda62e01-fced-4c35-8f18-952fe3cf1434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": -2,
                "shift": 8,
                "w": 10,
                "x": 77,
                "y": 25
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "60b25bd0-ae25-404d-9218-2a725f2d8c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 218,
                "y": 25
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "72895d92-d837-4eca-8007-d5ba28f74a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 3,
                "shift": 7,
                "w": 3,
                "x": 116,
                "y": 71
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4ab20186-79ec-49d5-b0e6-7d9b4f03acf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "7a9cf940-3a90-40c7-97be-42883ea86384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 16,
                "y": 71
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a9220351-aeea-49f9-8487-687e2a19ea12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 228,
                "y": 25
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "e39724f9-19c7-48c1-b322-1edec0aac12d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 101,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "02f5fb15-22a5-4baa-be67-d113315abafb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 161,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "63039dd9-9ddf-4e89-a129-be5a2db4e2d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 128,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "01ad56f0-8787-4779-b7ca-79b03760c043",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 241,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "0b6114e8-4c3e-49a9-ba10-00c0c083fe5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 119,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "543e380f-186b-430d-83cd-57b59d5eeb11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 126,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "234d4291-92f8-42c5-9493-2f02f3b162de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 131,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "624ecbf4-4778-4adb-ae4d-3f10d977fef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 48
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "c3ceeabe-c01e-4a3b-be4b-ea9d418167b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 225,
                "y": 48
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6920e97e-d0e7-4271-8ff3-3d97cce9190d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 238,
                "y": 25
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "d181ca4b-9140-400d-a2b2-a31b84e4448d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 51,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "82ef61f5-564a-4f1a-baf0-a9ad180e2713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 113,
                "y": 25
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "6e20c74c-0a68-4c48-a28e-e6349351869e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "29064051-0f75-4558-bf54-77ece5b9b040",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "feca42c9-afec-4c72-9be5-c5f16bae3ae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "5f8acf1d-4990-413f-bb6b-b35e016077fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8e4e71d6-e878-43f4-8ebf-7131d025ebd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 188,
                "y": 2
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3ab3ddb4-9744-46f7-b76e-250c89d761cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 41,
                "y": 25
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "f3c43e5a-4859-4237-85f4-3fb68c579389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "bb43e995-9add-4427-9b7d-40558ff42bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 28,
                "y": 25
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "5dbc46b7-9b56-4cf6-b719-ca9cbf8138b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 145,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "40bfe93c-c98b-4dec-bfb0-a948a80c0039",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 125,
                "y": 25
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "45aee3ec-be04-46a9-9a9f-eadfe0179832",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 12,
                "x": 203,
                "y": 2
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "005aee25-7c8c-4965-92e6-150646541b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 53,
                "y": 25
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "9833581f-8845-4ba0-b737-057a4fd2a912",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 158,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "51a74238-86b7-4b76-87cb-3768c1640e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 15,
                "y": 25
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "4a8159e0-2e39-42d5-a8da-1c7274b287fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 21,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "8c85fd65-b202-4f69-ab04-ca9a72dc8a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 147,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a8bf1b1a-054e-4ed9-92cc-436a930e9e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 17,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "78eaf042-bbf4-411b-a076-272cc8a866ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 65,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "82c168dd-ed41-4501-bfe2-0ce889b5b63f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "dfb48941-81d8-4a80-8027-45236aef180c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "0325556f-0dc4-45b6-8f3f-f9aa1f0a2326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "4a471b30-c4a4-4744-af07-5244ea2fa48d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 89,
                "y": 25
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e5a0338f-bd9f-41df-82d2-5536b0c48923",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 37,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "40278111-a8ed-42b3-8dcd-b7140d168cd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 101,
                "y": 25
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fa021bae-7f53-4630-8bfc-88c2930d6084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "b60a416d-2cd5-4d7a-a31c-6dce6da3ec1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 230,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "beef37ff-b658-4d2c-9b5a-de40404ba1e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 101,
                "y": 71
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "59094c1e-9dca-40ed-a5f1-0971159d17df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 11,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "f4d7c757-b35b-4cc9-80a4-ac9f9294c6cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 96,
                "y": 71
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "38b161fa-7dc0-464c-aaf4-c7109b5094a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 65,
                "y": 48
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "c866792f-5624-4b3b-a809-9dc21e7f42e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 158,
                "y": 25
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "45852693-d650-46fc-a211-612b00b6ce30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 2,
                "shift": 5,
                "w": 3,
                "x": 86,
                "y": 71
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "74953a79-16c3-4029-a601-b8fb62638163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 145,
                "y": 48
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "4bf3e709-94ba-4a1b-bba8-b9d6af43a298",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 56,
                "y": 48
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2c3551ae-d0d0-4ba8-82d2-03585f871ba7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 233,
                "y": 48
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2df55b7a-9fbc-4da6-9c66-3ff012bcc3d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 47,
                "y": 48
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "884c7be6-31da-4001-9731-82c4b515b221",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 38,
                "y": 48
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "2bd24f9c-4b45-4b71-b78c-dea76ccf3fba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "946d5db2-8c5b-4079-a8b6-14745cb80271",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 168,
                "y": 25
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "245b8070-ae52-4b33-8750-ff6614f31053",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 185,
                "y": 48
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "52520ab7-89c4-4694-81af-68047d9a0522",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 3,
                "x": 91,
                "y": 71
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "8d84bd8e-3797-4028-b20a-12e9e0d80bd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -3,
                "shift": 3,
                "w": 5,
                "x": 9,
                "y": 71
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "8624e546-5715-4b5c-87ed-9a6bf546feef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 7,
                "x": 11,
                "y": 48
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "17ab210f-3621-4f1d-b696-bc80d8aa3473",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 3,
                "w": 2,
                "x": 153,
                "y": 71
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "4a1f3ab8-913f-4782-97c0-8a8c0344c5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5c9749d3-9e33-47e6-9b80-1601b8be932c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 193,
                "y": 48
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "83512afa-d72b-4175-8791-18e58b068230",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 48
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "e6b412b9-30c6-4e1b-8371-004696fde934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "c09d2ba9-bdc5-4e8c-9303-1eca74ff8be6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 20,
                "y": 48
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "219e4799-2a56-4b75-a5f9-be934a8f57ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 5,
                "x": 23,
                "y": 71
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "62120d3a-c459-4d3f-b2e2-fa052eb930dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 169,
                "y": 48
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a0c6da59-f998-42a5-b46f-25834e088fa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 30,
                "y": 71
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "031f4d00-b012-4e3d-a4a2-c5234fe2eab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 209,
                "y": 48
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "5a5f92c8-89ef-45f3-b645-d1db37ae3cdc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 217,
                "y": 48
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "19d9b0a0-4ba4-4232-bafa-b26e05d80da1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 188,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1adb9f82-73ac-4a50-8103-d00449bf838f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 153,
                "y": 48
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e6605719-d56e-4655-8167-179c69e301bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 92,
                "y": 48
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d32ba8e4-eb5c-4e60-8271-321bb8f79554",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 177,
                "y": 48
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d411f71c-e817-4ecf-a862-a80ce2fbabed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 76,
                "y": 71
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "724b61bd-6dd4-4511-a2a3-17cfdd0183ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 3,
                "shift": 7,
                "w": 2,
                "x": 149,
                "y": 71
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "bb230e38-c5f4-4d39-8365-184f0f1b17d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 121,
                "y": 71
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "7a2788c5-85ad-4e1b-85b3-6786d34603a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 198,
                "y": 25
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 10,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}