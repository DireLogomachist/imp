{
    "id": "d735e97e-abaf-4e3b-aaf8-0936ef1dc158",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fn_tinypixel",
    "AntiAlias": 1,
    "TTFName": "${project_dir}\\fonts\\fn_tinypixel\\tinypixel.otf",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "tinypixel",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "41af1282-e3c7-4ef4-a877-c47677f4ad74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 132
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "18e36d76-aaa3-4b84-97ae-f4301d7e37cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 34,
                "y": 132
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "588eb30c-5852-4553-a987-d2954d798e6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 14,
                "y": 106
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f2447fff-f15a-4524-9bc7-fba417f03d56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "5ce88de1-70c1-44f1-9830-e0de3c99d05b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 80
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4044b287-b54a-4708-a9c8-e06e92abcfa6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "1809def1-cdb5-4d53-a349-221acb7e0015",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 24,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 62,
                "y": 80
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "8c6d80d5-4a5a-4682-8894-6bfa7ce6773a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 10,
                "y": 132
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9560f75b-baf5-451c-8a2f-cf30d6216fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 239,
                "y": 106
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "8ab58bbc-157e-48f8-bf5e-e5eb2af7ead9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 230,
                "y": 106
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "03fe8fa8-6e06-4e15-88be-0d5e25a3c97d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 26,
                "y": 106
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "aaec1ac7-9505-43a5-afb8-2565a9126e6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 106
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "936b8f94-422d-4f76-b573-6fde6e057da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 22,
                "y": 132
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "70481f65-8674-434a-be4a-2630654c39bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 242,
                "y": 80
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "299f7fff-6d92-41e1-b6ff-be75d0cc05b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 58,
                "y": 132
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "9616e5d2-48b5-4c19-82a2-9de0dfb2608f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "2e27ba92-e0fc-427e-9b9e-7bed5a1aa1c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 80
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "92d7698b-39e1-4011-8b7d-b0893896fe60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 134,
                "y": 106
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "ea3768d3-d1ae-4060-bba4-15342551192a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 182,
                "y": 54
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "415681ef-caa9-4498-97b2-af0a1b7b3791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 167,
                "y": 54
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f2881aca-fc08-43b3-ac12-fb30474eba16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 54
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "799177f3-60b7-4e1c-9652-0e91ed569f37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 227,
                "y": 54
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "26d08e82-59c8-4e84-a752-83a2f4a82088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "d178aa9f-308a-46ce-b5a0-7e688f9ecf41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 80
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3759dded-65ea-4b6a-afd2-78c0e25f39bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 80
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "86a6dae5-3bf3-4216-bc52-0c6201100c0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 80
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "9aff4446-6d56-47d1-8060-177e75e0a10e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 16,
                "y": 132
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7fad2efe-8de9-4c98-899b-8a0e7d3e51bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 52,
                "y": 132
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a64eee6a-3fd3-4925-98b5-6ea27b144ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 170,
                "y": 106
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "87f3c3a4-1274-4221-b505-446e82ba7674",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 158,
                "y": 106
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "c1853bee-a621-425e-89ad-68c672468bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 146,
                "y": 106
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "44c81189-633d-4d4b-a3ac-e57e138f683b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 80
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "98502b94-4c7f-452c-ae7b-142220529792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 80
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "797a70e6-6a5b-4626-86ed-1a6e366ec733",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 182,
                "y": 80
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e43b823e-89d9-4879-99da-319f18f29777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 80
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "1d7b0ab2-c497-4786-bdb4-510e065867cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 227,
                "y": 80
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b0cd6c00-f2ba-451a-8409-e6ad7d0dd821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 197,
                "y": 80
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4225f129-99bc-477f-a6f5-9a468a48077c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 167,
                "y": 80
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "59ce3277-d0db-47ed-bd8b-394c1172ed85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 152,
                "y": 80
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "87e45840-ff86-466f-9b48-c3164e7c983b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 137,
                "y": 80
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "c1145fc0-e39d-4926-bb3c-d316d156cdc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 197,
                "y": 54
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "b86d8e1c-9432-4063-a5d5-a27a23d8c8db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 98,
                "y": 106
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2cad0ef0-f728-4fa0-9316-30611a3ee559",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 137,
                "y": 54
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "df85b2cf-c694-4beb-8d37-95c080df5300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 54
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "8d08373d-7e1c-47d9-bb7d-117f47e6d862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 28
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ac6e501b-0a21-4c0a-a250-ec40f2db7981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 20,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "05b0658d-f0e8-4750-b248-2d4ae3e7fc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 28
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "67b0f48d-25b2-446b-ad79-92e201001f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 28
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "d2225d34-f9cf-41b9-9527-0c686f37af4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 28
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a0103c15-40f1-4581-a867-49086b442ea2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 28
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "ec82f1d5-3873-4077-af98-2ab5e46741a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 28
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "7b7c031d-9b1d-41e8-b41b-9ad953f92e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 28
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "415a3855-659f-4676-a46a-53ef766c1f74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 28
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b3dcea28-27a0-4c23-9723-880b608a7bf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "d3b76d43-2629-46a3-9559-59fe0943265e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "3584636e-a647-446c-a833-6cb0665fcfed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2ddad0f7-95f7-4a5a-9238-4c619a8f2464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 110,
                "y": 106
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "848fda12-c221-44cf-9175-d99c5f30b1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "800b0e30-0885-4a19-a2fe-bb6a26827e11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "7af2722d-62ed-4b25-8637-60ac86f2cc7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 221,
                "y": 106
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "e5f898a9-dcf5-4440-9e6d-b438e6a55465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "baed1ab7-21c1-4ce4-81da-0f23c4f360fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 212,
                "y": 106
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "31cce755-a645-4895-bf32-02f6a4fb699f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "75eb67fa-f66f-4e44-ae5a-c4543838ba17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 54
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "031c132e-df88-42a5-b421-9a6a6b400ab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 203,
                "y": 106
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "5f703634-2efd-42a9-86d2-72310b5c293e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 137,
                "y": 28
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7473c09e-5637-458d-83d5-4681334741bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 54
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "5d2b47c2-44fc-4923-a930-99cc14bb3f5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 28
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "23e20479-9e9d-408c-a471-43b9c504cb09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 54
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a9520b28-392c-4e6e-9769-b2e477d2cefc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 54
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d9e87ad1-9a0c-4d4b-a43d-c93258fe01ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 182,
                "y": 106
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "95c4a6ee-a61f-4888-96c8-6d91b5522d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 54
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "8798b944-d41f-407a-9c0c-47a0ab2369c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 54
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "1fe5ea07-4f28-436a-ab2f-2e2e9b3dd53f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 40,
                "y": 132
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "94c609db-1c93-4512-951e-7406d389b8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 24,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 194,
                "y": 106
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "699aebc7-1f6c-440e-8e40-6f13e6f6153e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 62,
                "y": 106
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1d44fdf1-1b3d-4a20-b3b4-62051e8adee8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 28,
                "y": 132
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "e471e91f-4a76-40df-8980-a9ed52c3a6da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "35fdead9-160e-400b-8ea4-9ec4957108da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 54
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5801bf63-be47-47c4-946d-fab122e327b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 54
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "94380d04-8447-4449-b603-1ff7e8b4eb37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 227,
                "y": 28
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b8bf9272-eba3-40f5-b1a9-2713790b25c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 212,
                "y": 28
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "31c41815-18ed-4f25-918a-5d0f9349e088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 38,
                "y": 106
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "3c5b069f-e840-4cbf-87d5-95c4f28e9833",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 50,
                "y": 106
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9a000472-292a-46d9-bf59-ddc29fab95bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 86,
                "y": 106
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8d2a8714-3f01-4928-9e63-d3a43015a686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 197,
                "y": 28
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "80f1a770-ef2f-46a1-ba46-77f9dfae9e75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c4589363-eb39-4cbd-b681-8043869ad6ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 24,
                "offset": 0,
                "shift": 18,
                "w": 16,
                "x": 74,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "399dc86e-d1a3-4cbd-a5df-c9eb2cdffd3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 182,
                "y": 28
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "dd9eee40-0184-41c3-bc2c-ce9dc50812f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 167,
                "y": 28
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "319d8fa0-7967-49f7-8a3d-22981ad61b47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 152,
                "y": 28
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "82b93666-0739-46b8-b0d3-fae5159a0753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 74,
                "y": 106
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f2e3d214-a673-4c4b-8a76-85723463f474",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 24,
                "offset": 0,
                "shift": 6,
                "w": 4,
                "x": 46,
                "y": 132
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "741627e0-fbd6-4592-ac7a-3933aeccff4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 24,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 122,
                "y": 106
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "4bdadff8-d34f-47bb-bdc1-552d4233c131",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 24,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 152,
                "y": 54
            }
        }
    ],
    "image": null,
    "includeTTF": true,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}