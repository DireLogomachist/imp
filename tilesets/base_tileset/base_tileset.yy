{
    "id": "c6f729d4-70ce-4c0b-9100-ef01b3cb81ee",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "base_tileset",
    "auto_tile_sets": [
        {
            "id": "55fb3b7a-e767-4cb8-b9d1-d2e9d1a90bfa",
            "modelName": "GMAutoTileSet",
            "mvc": "1.0",
            "closed_edge": false,
            "name": "autotile_1",
            "tiles": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ]
        }
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 1,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "9626ee61-2489-4d8f-a39d-cbd0b528c362",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 2,
    "tileheight": 32,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}