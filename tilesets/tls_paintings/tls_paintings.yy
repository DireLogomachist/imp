{
    "id": "95086a67-1e5c-4be2-a35b-a67585788563",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tls_paintings",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 2,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "945f5b06-310d-4751-b802-f6d6cad7f9de",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 6,
    "tileheight": 32,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 32,
    "tilexoff": 0,
    "tileyoff": 0
}