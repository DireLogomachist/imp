{
    "id": "b30b5618-b9bd-45da-a694-905c27cbdd20",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "laser_u",
    "eventList": [
        {
            "id": "0c989ab3-3f09-4e37-8845-425f5adb987a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b30b5618-b9bd-45da-a694-905c27cbdd20"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "c2982aa3-6bd6-4faa-a39f-415dc6b5f28c",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7",
            "propertyId": "b4236acc-a8f2-4530-a314-81cee7037bb0",
            "value": "270"
        }
    ],
    "parentObjectId": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "0fbbb2dd-23e9-42c7-856d-0350a6e51e81",
    "visible": true
}