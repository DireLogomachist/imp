{
    "id": "53e1d45f-6acd-4563-b559-bedc8cb40dfc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "locked_door",
    "eventList": [
        {
            "id": "e3cbfa2d-91f1-4dcc-837f-1e20cf688386",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "53e1d45f-6acd-4563-b559-bedc8cb40dfc"
        },
        {
            "id": "3b5694de-c6d8-4971-892b-a5cb51bd4ab5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "53e1d45f-6acd-4563-b559-bedc8cb40dfc"
        },
        {
            "id": "4db1054a-9696-443a-a64e-c152430a59da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "53e1d45f-6acd-4563-b559-bedc8cb40dfc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "da97011b-3be2-49ff-9d64-016183312b04",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
    "visible": true
}