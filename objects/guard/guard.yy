{
    "id": "faf6bd27-ff07-4009-ad26-09becbd7f715",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "guard",
    "eventList": [
        {
            "id": "8ea314ab-aad3-4808-aa67-18ceb2794df7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "f05070b6-b165-4bb9-a19b-e8ad1b81a1e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "d268158f-75f8-48d9-9b4e-bc6fa10bbb73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "db705532-65eb-428e-8a72-a92b81293ce1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "2e517a1c-8a34-49c5-b634-a4359d1f748a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "cb5faf0c-d424-44c9-a4c9-b5cbbfa6912c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "7af51b1b-3ba9-46d3-aa81-863ebf980622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        },
        {
            "id": "d6ac414b-dfb7-470d-b6b0-c56727dd9c97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "faf6bd27-ff07-4009-ad26-09becbd7f715"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "70140ca2-1fe4-4e12-9949-4297a3240c27",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
    "visible": true
}