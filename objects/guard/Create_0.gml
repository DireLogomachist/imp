/// @description guard init


event_inherited()

attack_radius = 280;

fire_available = true;
fire_countdown = 1*room_speed;
alert_wait = .2*room_speed;

gun = instance_create_layer(x, y, "Entities", guard_gun);
gun.visible = false;
