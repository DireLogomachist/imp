{
    "id": "f35895fc-6a28-42e8-abff-bf95cae2e9c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gui_button_space",
    "eventList": [
        {
            "id": "e0462d8e-b0b5-4794-a383-c86dbfb8af69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f35895fc-6a28-42e8-abff-bf95cae2e9c9"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "425404f1-99fa-4c0f-830d-ec292d4682fc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c94dc038-d6a4-4cf8-bdb7-fb1a88a0cf53",
    "visible": true
}