{
    "id": "f24662ee-0127-4f0a-b94b-59db0bc00e01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "fade_out_controller",
    "eventList": [
        {
            "id": "3f0c3dc5-225f-4f8c-a863-fadf3a3b9334",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f24662ee-0127-4f0a-b94b-59db0bc00e01"
        },
        {
            "id": "90497a44-7de6-46b6-b136-92fa51902ae1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f24662ee-0127-4f0a-b94b-59db0bc00e01"
        },
        {
            "id": "defa1b4d-ef1e-40c3-b849-76b9f05a4c65",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f24662ee-0127-4f0a-b94b-59db0bc00e01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}