/// @description 


var orig_color = draw_get_color();
var orig_alpha = draw_get_alpha();
var progression = 1 - (alarm[0]/fade_duration);
if (alarm[0] < 0) progression = 1.0;

draw_set_alpha(progression);
draw_set_color(fade_color);

draw_rectangle(0, 0, global.window_w, global.window_h, false);

draw_set_alpha(orig_alpha);
draw_set_color(orig_color);
