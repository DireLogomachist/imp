{
    "id": "f89267f6-dcf0-4f9b-8057-bdd3e1f7e7ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vent_entrance_left",
    "eventList": [
        {
            "id": "3407d834-a273-4f33-a3d9-4d5df648d243",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f89267f6-dcf0-4f9b-8057-bdd3e1f7e7ff"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "8d8abb05-5625-4c91-8f08-f7544530be26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0d282d1a-727e-477a-b151-05ab19a682b8",
    "visible": true
}