/// @description 


menu_pos_x = global.window_w/2;
menu_pos_y = global.window_h/2 + 100;
menu_y_offset = 25;
menu_x_offset = 180;

selected_index = 0;

main_menu[0] = "START";
main_menu[1] = "LEVEL SELECT";
main_menu[2] = "OPTIONS";
main_menu[3] = "CREDITS";
main_menu[4] = "QUIT";

submenu_levels[0] = "LOBBY";
submenu_levels[1] = "FOYER";		//lasers
submenu_levels[2] = "ANTECHAMBER";	//guards
submenu_levels[3] = "HALL";			//move_fast
//submenu_levels[4] = "MAINTENANCE";	//pipes
submenu_levels[4] = "RECEPTION";	//circles
submenu_levels[5] = "TOWER";		//foyer
submenu_levels[6] = "GALLERY";		//gallery
submenu_levels[7] = "PENTHOUSE";
submenu_levels[8] = "BACK";

submenu_options[0] = "LOL";
submenu_options[1] = "I GOT NOTHING";
submenu_options[2] = "SORRY";
submenu_options[3] = "BACK";

submenu_credits[0] = "ANDY MILLER";
submenu_credits[1] = "TWITTER: @andy_codes";
submenu_credits[2] = "Music track \"Menu\" by Thor Arisland"
submenu_credits[3] = "Music track \"Driving\" by Thor Arisland"
submenu_credits[4] = "BACK";

active_menu = main_menu;
submenu_list[0] = noone;
submenu_list[1] = submenu_levels;
submenu_list[2] = submenu_options;
submenu_list[3] = submenu_credits;

red_color = $282FFF;
draw_set_color(red_color);
draw_set_font(fn_tinypixel);
draw_set_halign(fa_center);

audio_play_sound(au_menu_music, 0, true);
