/// @description 


var menu = active_menu;

if array_length_1d(menu) < 6 {
	for(var i = 0; i < array_length_1d(menu); i++) {
		var entry = menu[i];
		if (i == selected_index) entry = "> " + entry + " <";
		draw_text(menu_pos_x, menu_pos_y + i*menu_y_offset, entry);
	}
} else {
	for(var i = 0; i < array_length_1d(menu); i++) {
		var entry = menu[i];
		if i < ceil(array_length_1d(menu)/2) {
			var x_off = -menu_x_offset/2;
			var y_reset = 0;
		} else {
			var x_off = menu_x_offset/2;
			var y_reset = ceil(array_length_1d(menu)/2);
		}
		if (i == selected_index) entry = "> " + entry + " <";
		draw_text(menu_pos_x + x_off, menu_pos_y + (i-y_reset)*menu_y_offset, entry);
	}
}
