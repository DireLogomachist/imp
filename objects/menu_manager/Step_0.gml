/// @description 


if keyboard_check_pressed(vk_up) and selected_index !=0 {
	selected_index--;
	audio_play_sound(au_menu_blip, 0, false);
}

if keyboard_check_pressed(vk_down) and selected_index != array_length_1d(active_menu)-1 {
	selected_index++;
	audio_play_sound(au_menu_blip, 0, false);
}

if keyboard_check_pressed(vk_escape)
	game_end();
	
if keyboard_check_pressed(vk_space) or keyboard_check_pressed(vk_enter) {
	audio_play_sound(au_menu_blip, 0, false);
	if active_menu == main_menu {
		if selected_index == 0 {
			global.flags[?"menu_started"] = true;
			global.gm.lvl_index = 1;
			audio_stop_sound(au_menu_music);
			room_goto(global.gm.levels[1]);
		} else if selected_index == array_length_1d(active_menu)-1 {
			game_end();
		} else {
			active_menu = submenu_list[selected_index];
			selected_index = 0;
		}
	} else if active_menu != main_menu and selected_index == array_length_1d(active_menu)-1 {
		active_menu = main_menu;
		selected_index = 0;
	} else if active_menu = submenu_levels {
		global.gm.lvl_index = selected_index+1;
		audio_stop_sound(au_menu_music);
		room_goto(global.gm.levels[selected_index+1]);
	} else if active_menu == submenu_options {
		
	} else if active_menu == submenu_credits {
		if (selected_index <= 1) 
			url_open("https://twitter.com/andy_codes");
		if (selected_index == 2)
			url_open("https://opengameart.org/content/menu-1");
		if (selected_index == 3)
			url_open("https://opengameart.org/content/driving");
	}
}
