{
    "id": "a6607b42-a6b7-4b32-9596-064162feb34d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "guard_gun",
    "eventList": [
        {
            "id": "38e02148-1b08-44d2-9829-d5b4070a739d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a6607b42-a6b7-4b32-9596-064162feb34d"
        },
        {
            "id": "3d08be13-6795-4504-aff5-0b82f2cd9cc6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a6607b42-a6b7-4b32-9596-064162feb34d"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
    "visible": true
}