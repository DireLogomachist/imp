/// @description 


var vec = dir_to_vec(point_dir);
x = enemy_pos[0] + vec[0]*radius;
y = enemy_pos[1] - vec[1]*radius;
muzzle_x = enemy_pos[0] + vec[0]*(radius+5);
muzzle_y = enemy_pos[1] - vec[1]*(radius+5);

image_index = clamp_dir(point_dir+22.5)/45;
