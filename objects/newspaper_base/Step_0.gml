/// @description 


if elapsed > duration*1000000
	running = false;

if running == true {
	var t = elapsed/(duration*1000000);
	x = lerp(x, end_pos[0], t*t * (3 - 2*t));
	y = lerp(y, end_pos[1], t*t * (3 - 2*t));
	elapsed  = get_timer() - start_time;
}
