{
    "id": "dad24f1b-f5bc-4a4d-9680-cf0ec44aad04",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "elevator_doors_exit",
    "eventList": [
        {
            "id": "034d2365-7dd9-420a-839e-fe2f1f914a62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dad24f1b-f5bc-4a4d-9680-cf0ec44aad04"
        },
        {
            "id": "f136991e-93de-45e3-b963-e1485baa2622",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "dad24f1b-f5bc-4a4d-9680-cf0ec44aad04"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
    "visible": true
}