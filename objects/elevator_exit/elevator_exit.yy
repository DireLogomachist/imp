{
    "id": "c030b41f-1b8e-4063-adbb-350bd8198281",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "elevator_exit",
    "eventList": [
        {
            "id": "6b0ce372-c4b7-44d0-9925-26a5fc77092d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c030b41f-1b8e-4063-adbb-350bd8198281"
        },
        {
            "id": "e728b780-6fa8-4c48-b585-2407ca924f3d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c030b41f-1b8e-4063-adbb-350bd8198281"
        },
        {
            "id": "43efd0bb-5a73-47ea-b248-884b81ea8e27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "c030b41f-1b8e-4063-adbb-350bd8198281"
        },
        {
            "id": "76d3250c-8fe3-470a-abe8-8d84e82b66ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "c030b41f-1b8e-4063-adbb-350bd8198281"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "58cff814-8072-47e7-9ad8-15d6e1536d8b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}