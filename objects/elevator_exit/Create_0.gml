/// @description 


event_inherited();
action = elevator_exit_level;

fade_delay = .5*room_speed;
exit_delay = 1.5*room_speed;

// Spawn elevator doors
doors = instance_create_layer(x, y - 32, "Elevators", elevator_doors_exit);
doors.image_index = 5;
button = instance_create_layer(x-30, y, "Vents", elevator_button);
