{
    "id": "c68d4907-3682-4939-b327-fb234db052cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vent_path",
    "eventList": [
        {
            "id": "535ff635-646a-4473-b064-57e25f5bd206",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c68d4907-3682-4939-b327-fb234db052cd"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "cbace74c-f498-4d4b-ad19-bd063dac48b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
    "visible": true
}