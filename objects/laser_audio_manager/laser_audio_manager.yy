{
    "id": "d4284c89-3cde-4b5f-b22b-6ecd5eab91f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "laser_audio_manager",
    "eventList": [
        {
            "id": "b5fe6d07-c332-4a12-a103-ff23f793c625",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4284c89-3cde-4b5f-b22b-6ecd5eab91f2"
        },
        {
            "id": "1b56355b-744f-4dfc-af24-37c9d557afdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 12,
            "m_owner": "d4284c89-3cde-4b5f-b22b-6ecd5eab91f2"
        },
        {
            "id": "fed58e92-59e6-4511-be60-baf16f39feeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "d4284c89-3cde-4b5f-b22b-6ecd5eab91f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}