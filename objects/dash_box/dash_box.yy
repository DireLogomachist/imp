{
    "id": "93041b3e-8194-4a1e-b4d3-e03f11e7c5fd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "dash_box",
    "eventList": [
        {
            "id": "26784b69-7781-4f53-a392-dbb292fd990c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "93041b3e-8194-4a1e-b4d3-e03f11e7c5fd"
        },
        {
            "id": "0198b500-b67a-4a1c-b1cc-dd44f3dcb748",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "93041b3e-8194-4a1e-b4d3-e03f11e7c5fd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ba937f1c-3cfd-4d9a-8abc-321e0d91cdce",
    "visible": true
}