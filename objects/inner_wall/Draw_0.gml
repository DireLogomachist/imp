/// @description


if global.player_inst.state_name == "Vent" or global.player_inst.state_name == "VentDash"
	shader_set(darken_all_shader);
	
draw_self();

if global.player_inst.state_name == "Vent" or global.player_inst.state_name == "VentDash"
	shader_reset();
