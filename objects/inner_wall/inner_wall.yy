{
    "id": "6c4ecbe1-7c51-4a86-a633-08ad291c3fba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "inner_wall",
    "eventList": [
        {
            "id": "42f39c52-e8ab-49e4-bdb2-88c4329c0f9b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6c4ecbe1-7c51-4a86-a633-08ad291c3fba"
        },
        {
            "id": "00f9d007-420a-4a05-849a-42c84a44611c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "6c4ecbe1-7c51-4a86-a633-08ad291c3fba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "da97011b-3be2-49ff-9d64-016183312b04",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "322435db-8f4f-4492-8f5f-dc6a3f295c2f",
    "visible": true
}