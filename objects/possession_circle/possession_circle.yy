{
    "id": "0c18de62-5a72-4b85-9544-c42e8c08df4a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "possession_circle",
    "eventList": [
        {
            "id": "a00ab1ba-c30b-476d-b3d4-2dc255a198bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0c18de62-5a72-4b85-9544-c42e8c08df4a"
        },
        {
            "id": "61538708-3008-4035-9895-1aeedd247a18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0c18de62-5a72-4b85-9544-c42e8c08df4a"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a041ceed-a5b1-4552-a81b-09ae239cff92",
    "visible": true
}