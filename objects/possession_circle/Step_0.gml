/// @description 

elapsed = (get_timer()-timestart)/1000000

if elapsed < duration {
	image_alpha = sin(elapsed/(duration)*pi)
} else {
	instance_destroy()
}
