{
    "id": "43e5d18e-b08c-43a5-93c2-2e28a2092e8e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "fade_in_controller",
    "eventList": [
        {
            "id": "5a2b1edb-28c8-4c3e-8102-30b72cfb492a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "43e5d18e-b08c-43a5-93c2-2e28a2092e8e"
        },
        {
            "id": "2e912f10-7569-4f12-9550-4f0626a20d36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "43e5d18e-b08c-43a5-93c2-2e28a2092e8e"
        },
        {
            "id": "42420059-b482-4126-a041-b8da2b869e63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "43e5d18e-b08c-43a5-93c2-2e28a2092e8e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}