{
    "id": "11bf30e5-a692-4f4b-83a9-13943d677b11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "shadow",
    "eventList": [
        
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "695949e2-72e4-49b4-bcc4-90ff13a0365c",
    "visible": true
}