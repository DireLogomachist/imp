/// @description base enemy init


event_inherited();

player_controlled = false;
patrol_speed = move_speed/6;
hop_amp = 2.5;
hop_freq = 38.2;

scan_dist = 400;
scan_radius = 70;
scan_num_rays = 10;
raycasts[0,0] = 0;
reset_pause_duration = 1.5*room_speed;

patrol_path = noone;
pursuit_path = noone;
reset_path = noone;
draw_rays = false;
//hitwall = noone;

state_machine_init();
state_create("Idle", enemy_idle);
state_create_entry("Idle", enemy_idle_entry);
state_create("Patrol", enemy_patrol);
state_create_entry("Patrol", enemy_patrol_entry);
state_create_exit("Patrol", enemy_patrol_exit);
state_create("Alert", enemy_alert);
state_create_entry("Alert", enemy_alert_entry);
state_create("Attack", enemy_attack);
state_create_entry("Attack", enemy_attack_entry);
state_create("Pursuit", enemy_pursuit);
state_create_entry("Pursuit", enemy_pursuit_entry);
state_create("ResetPause", enemy_reset_pause);
state_create("Reset", enemy_reset);
state_create_entry("Reset", enemy_reset_entry);
state_create("Possessed", enemy_possessed);
state_create_entry("Possessed", enemy_possessed_entry);

state_init("Idle");

convert_patrol_path(patrol_str);
