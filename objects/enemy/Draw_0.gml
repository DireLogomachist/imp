/// @description 

if global.player_inst.state_name == "Vent" or global.player_inst.state_name == "VentDash"
	shader_set(darken_all_shader);

if draw_rays and not player_controlled {
	for (var i=0; i<array_height_2d(raycasts); i++;) {
		var r = raycasts;
		var c = c_lime;
		if raycasts[i,4] c = c_red;
		draw_line_width_color(r[i,0], r[i,1], r[i,2], r[i,3], 1, c, c);
	}
	//if hitwall != noone
	//	draw_circle_color(hitwall[0], hitwall[1], 5, c_red, c_red, false);
	//hitwall = noone;
}

event_inherited();

if global.player_inst.state_name == "Vent" or global.player_inst.state_name == "VentDash"
	shader_reset();
