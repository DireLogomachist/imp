{
    "id": "c884c364-f97c-44d0-a5b8-f1a3817e7215",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gui_button_a",
    "eventList": [
        {
            "id": "63ce3ba3-4dc3-4d29-8e77-f131cfb92264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c884c364-f97c-44d0-a5b8-f1a3817e7215"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "425404f1-99fa-4c0f-830d-ec292d4682fc",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "05ee7a46-dc3f-490f-8562-bbff85c864b4",
    "visible": true
}