{
    "id": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "laser_l",
    "eventList": [
        {
            "id": "073ad87f-f2e9-4fa9-b6c7-2e0f8e402617",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7"
        },
        {
            "id": "42ce8144-0bcd-45e9-94e4-a3e14243a291",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7"
        },
        {
            "id": "8f945cb9-4cc8-4a41-952f-ed36526cdbcd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        {
            "id": "b4236acc-a8f2-4530-a314-81cee7037bb0",
            "modelName": "GMObjectProperty",
            "mvc": "1.0",
            "listItems": null,
            "multiselect": false,
            "rangeEnabled": false,
            "rangeMax": 10,
            "rangeMin": 0,
            "resourceFilter": 1023,
            "value": "0",
            "varName": "laser_dir",
            "varType": 0
        }
    ],
    "solid": false,
    "spriteId": "f1e36853-b37e-4bdd-9a5f-9f9c8e77ca30",
    "visible": true
}