///@description


if global.player_inst.state_name == "Vent" or global.player_inst.state_name == "VentDash"
	shader_set(darken_all_shader);

draw_self();

if global.player_inst.state_name != "Vent" and global.player_inst.state_name != "VentDash" {
	var orig_alpha = draw_get_alpha();
	draw_set_alpha(alpha_blur);
	draw_line_width_color(x+offset_x, y+offset_y, wall_x, wall_y, width_blur, color_blur, color_blur);
	draw_circle_color(wall_x, wall_y, 3, color_blur, color_blur, false);
	draw_set_alpha(orig_alpha);

	draw_line_width_color(x+offset_x, y+offset_y, wall_x, wall_y, width_main, color_main, color_main);
	draw_circle_color(wall_x, wall_y, width_main, color_main, color_main, false);
}

if global.player_inst.state_name == "Vent" or global.player_inst.state_name == "VentDash"
	shader_reset();
