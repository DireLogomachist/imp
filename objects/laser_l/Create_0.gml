///@description


// object constants
offset_x = 0;
offset_y = 0;
inst_layer_depth = depth;

// laser color constants
color_main = c_red;
width_main = 1;
color_blur = c_red;
width_blur = 4;
alpha_blur = .3;

// use laser seed to get randomized (but consistent) laser direction
//laser_dir = 0;
//laser_dir = laser_dir + random_range(-10,10);

// get wall hit position
var raycast_px = x + lengthdir_x(2000, laser_dir);
var raycast_py = y + lengthdir_y(2000, laser_dir);
var wall_hit = raycast_check_walls(x, y, raycast_px, raycast_py);
wall_x = wall_hit[0];
wall_y = wall_hit[1];

// laser audio
if (!variable_global_exists("laser_audio_manager") or global.laser_audio_manager == noone) {
	audio_mgr = instance_create_layer(0, 0, "GUI", laser_audio_manager);
	global.laser_audio_manager = audio_mgr;
} else {
	audio_mgr = global.laser_audio_manager
}
