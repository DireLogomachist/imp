///@description


depth = inst_layer_depth - y;

var player_killable = global.player_inst.state_name != "Dash" and global.player_inst.state != "Dead";

if player_killable and global.player_inst.player_alive and collision_line(x+offset_x, y+offset_y, wall_x, wall_y, player, false, true) {
	audio_play_sound(au_laser_zap, 0, false);
	with global.player_inst { player_death(); }
}

if (audio_mgr != noone) {
	var dist = line_distance(global.player_inst.x, global.player_inst.y, x+offset_x, y+offset_y, wall_x, wall_y);

	var new_volume = -1;
	if (dist > audio_mgr.max_dist) {
		new_volume = 0;
	} else if (dist < audio_mgr.min_dist) {
		new_volume = 1;
	} else {
		new_volume = 1 - (dist/audio_mgr.max_dist);
	}

	if new_volume > audio_mgr.current_vol {
		audio_mgr.current_vol = new_volume;
		audio_sound_gain(audio_mgr.laser_audio, audio_mgr.current_vol, 0);
	}
}
