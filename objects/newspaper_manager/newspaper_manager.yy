{
    "id": "bdc5ae30-66bd-4b53-822e-972ab220fdd4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "newspaper_manager",
    "eventList": [
        {
            "id": "52b2fabf-220c-4900-834e-da6cf5d21b4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdc5ae30-66bd-4b53-822e-972ab220fdd4"
        },
        {
            "id": "0aad47a5-808c-4580-996c-a09d2a1338fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bdc5ae30-66bd-4b53-822e-972ab220fdd4"
        },
        {
            "id": "9be536ea-6cce-4c8b-a418-c891c344990f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "bdc5ae30-66bd-4b53-822e-972ab220fdd4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}