 /// @description 


newspapers[0] = inst_5D4065FF;
newspapers[1] = inst_28EB82C1;
newspapers[2] = inst_7B096DAA;
newspapers[3] = inst_6DF34505;
newspapers[4] = inst_A2B1400;
newspapers[5] = inst_3C2AC66D;

newspapers[0].end_pos = array(392, 38);
newspapers[1].end_pos = array(389, 34);
newspapers[2].end_pos = array(391, 40);
newspapers[3].end_pos = array(392, 42);
newspapers[4].end_pos = array(390, 48);
newspapers[5].end_pos = array(415, 25);

newspapers[0].alarm[0] = 4.0*room_speed;
newspapers[1].alarm[0] = 8.0*room_speed;
newspapers[2].alarm[0] = 13.0*room_speed;
newspapers[3].alarm[0] = 18.0*room_speed;
newspapers[4].alarm[0] = 24.0*room_speed;
newspapers[5].alarm[0] = 30.0*room_speed;
alarm[0] = 40*room_speed;

instance_create_depth(0,0,0,fade_in_controller);
audio_play_sound(au_credits_music, 0, true);
