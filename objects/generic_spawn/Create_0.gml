/// @description 


// Spawn player
var offset_x = 16;
var offset_y = 16;
with instance_create_layer(x, y, "Entities", player) {
	x = other.x + offset_x;
	y = other.y + offset_y;
}
