/// @description movable base object


player_controlled = false
move_speed = 160;

spr_hop_offset = 0;
hopping = false;
hop_amp = 4;
hop_freq = 25.5;

drop_dist_counter = 0;
dust_drop_dist = 50;
dust_drop_offset = 15;
last_frame_pos = array(x, y);

inst_layer_depth = depth;

movement_inputs[0] = vk_right;
movement_inputs[1] = vk_up;
movement_inputs[2] = vk_left;
movement_inputs[3] = vk_down;

face_direction();

obj_shadow = instance_create_layer(x, y, "Entities", shadow);
