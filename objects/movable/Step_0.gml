/// @description base moveable and player movement


// Set draw order based on height
depth = inst_layer_depth - y;
face_direction();

drop_dist_counter = drop_dist_counter + point_distance(x, y, last_frame_pos[0], last_frame_pos[1]);
last_frame_pos = array(x, y);

if hopping {
	spr_hop_offset = hop_amp*abs(cos(current_time/(hop_freq*pi)));
	if (drop_dist_counter > dust_drop_dist and state_name != "Possessing") {
		var dust_particle = instance_create_layer(x, y+6, "Splatter", dust);
		var vec = dir_to_vec(clamp_dir(180-facing_direction));
		dust_particle.x = dust_particle.x + vec[0]*dust_drop_offset;
		dust_particle.y = dust_particle.y + vec[1]*dust_drop_offset;
		drop_dist_counter = 0;
	}
} else {
	spr_hop_offset = 0;
}
