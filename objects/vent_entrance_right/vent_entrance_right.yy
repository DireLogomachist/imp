{
    "id": "ef39a59a-0f0e-4371-a876-840af295ef28",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vent_entrance_right",
    "eventList": [
        {
            "id": "de5d1f07-b5ad-4a1c-8561-c046a0efb076",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ef39a59a-0f0e-4371-a876-840af295ef28"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "8d8abb05-5625-4c91-8f08-f7544530be26",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb72618a-505f-4142-afcc-3ed89091d36a",
    "visible": true
}