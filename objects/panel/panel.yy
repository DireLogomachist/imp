{
    "id": "da44f5dd-690b-46d6-aa37-725ed04f71fb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "panel",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "58cff814-8072-47e7-9ad8-15d6e1536d8b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
    "visible": true
}