{
    "id": "aa41f798-9f47-4d1b-ac13-183abe3f17cd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "ui_alert",
    "eventList": [
        {
            "id": "1f4bba8e-ba10-4ac8-98fa-068ee26f0c2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aa41f798-9f47-4d1b-ac13-183abe3f17cd"
        },
        {
            "id": "693a94e7-7be9-4eba-a21a-8ca9513fd37b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aa41f798-9f47-4d1b-ac13-183abe3f17cd"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cd466780-f90c-48e8-b574-6ada3bc268f1",
    "visible": true
}