/// @description 


// Spawn elevator doors
instance_create_layer(x, y - 32, "Elevators", elevator_doors);

// Spawn player
var offset_x = 16;
var offset_y = 16 + 2;
with instance_create_layer(x, y, "Entities", player) {
	x = other.x + offset_x;
	y = other.y + offset_y;
	player_controlled = false;
}

// Open elevator doors
// Enable player movement
player_spawn_wait = 2.5*room_speed;
alarm[0] = player_spawn_wait;
