{
    "id": "619dd088-52f4-40ab-a3aa-43923730c16c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "wall_debugger",
    "eventList": [
        {
            "id": "6a1216d4-7e07-4e22-8c8c-48d7212c7c4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "619dd088-52f4-40ab-a3aa-43923730c16c"
        },
        {
            "id": "7f4e60a0-7a01-4807-ac4e-8257a6b4e923",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "619dd088-52f4-40ab-a3aa-43923730c16c"
        },
        {
            "id": "0d902385-5ac6-4af6-8d40-0c3e764636a2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "619dd088-52f4-40ab-a3aa-43923730c16c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cad37f6f-d083-4233-89fe-f93ff1e6c720",
    "visible": true
}