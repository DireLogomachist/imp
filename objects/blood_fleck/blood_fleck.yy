{
    "id": "335bdfc2-949e-49be-8247-702c43adf116",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "blood_fleck",
    "eventList": [
        {
            "id": "8760b07b-9028-465d-86d1-ca9ae72e3e38",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "335bdfc2-949e-49be-8247-702c43adf116"
        },
        {
            "id": "d352a686-ea07-4afa-8562-5320fb1369db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "335bdfc2-949e-49be-8247-702c43adf116"
        },
        {
            "id": "d7086f90-e59e-4688-9f28-7322c2130ac4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "335bdfc2-949e-49be-8247-702c43adf116"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
    "visible": true
}