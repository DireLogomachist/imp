{
    "id": "12cf42d7-5871-4a4b-b8a7-d6f62c084066",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "window",
    "eventList": [
        {
            "id": "61d8d4da-25ff-4588-a106-b8a808a5c82b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "12cf42d7-5871-4a4b-b8a7-d6f62c084066"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "da97011b-3be2-49ff-9d64-016183312b04",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9687deba-28d6-4f14-b691-1c22f6fe6788",
    "visible": true
}