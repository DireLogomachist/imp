{
    "id": "82966277-7e85-49ce-8c10-dc1bd7717e1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "exec",
    "eventList": [
        {
            "id": "a3f2f3b3-1cf9-44f1-a3ce-6a4c21545b72",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "82966277-7e85-49ce-8c10-dc1bd7717e1a"
        },
        {
            "id": "f9852331-c2d8-4618-9fc9-82848b4202b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "82966277-7e85-49ce-8c10-dc1bd7717e1a"
        },
        {
            "id": "b363a35e-fa9b-467c-a9c6-552980f89d8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "82966277-7e85-49ce-8c10-dc1bd7717e1a"
        },
        {
            "id": "65e8a483-a5ff-4fd5-88d1-b4a56e61b1cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 2,
            "m_owner": "82966277-7e85-49ce-8c10-dc1bd7717e1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "faf6bd27-ff07-4009-ad26-09becbd7f715",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
    "visible": true
}