/// @description trigger level load


with global.gm {
	lvl_index = lvl_index + 1;
	audio_stop_sound(au_room_background);
	room_goto(levels[lvl_index]);
}
