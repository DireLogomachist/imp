{
    "id": "3bf99486-11ff-49ce-9360-4293bb7e951a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "small_desk",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "80f4c4f6-dcf6-4243-943d-2fe4fa6825f8",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "23127a0e-0207-495f-adfd-bed0dbce3589",
    "visible": true
}