{
    "id": "80f4c4f6-dcf6-4243-943d-2fe4fa6825f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "desk",
    "eventList": [
        {
            "id": "d608d192-3ff7-46b7-92ca-3b7d193fbf1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80f4c4f6-dcf6-4243-943d-2fe4fa6825f8"
        },
        {
            "id": "60e9f80f-0765-4f74-9358-29df7885403c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "80f4c4f6-dcf6-4243-943d-2fe4fa6825f8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "da97011b-3be2-49ff-9d64-016183312b04",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "0c99145c-3e2a-4860-a425-7377ad2dd56a",
    "visible": true
}