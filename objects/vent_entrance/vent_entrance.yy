{
    "id": "8d8abb05-5625-4c91-8f08-f7544530be26",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "vent_entrance",
    "eventList": [
        {
            "id": "b4ab7f22-079d-4da2-996a-1563d6de216c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8d8abb05-5625-4c91-8f08-f7544530be26"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "cbace74c-f498-4d4b-ad19-bd063dac48b5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "46ed9c78-04da-4218-aeee-e314e8426e9e",
    "visible": true
}