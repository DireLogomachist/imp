/// @description 


if variable_global_exists("player_inst") and (global.player_inst.state_name != "Vent" and global.player_inst.state_name != "VentDash") {
	draw_set_color(red_color);
	draw_set_font(fn_tinypixel);
	draw_set_halign(fa_center);
	draw_text((x-view_x)*2, (y-view_y)*2, text_content);
}
