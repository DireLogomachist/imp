///@description player init


event_inherited();

global.player_inst = self;

player_alive = true;
player_controlled = true;
dash_hitbox = noone;
possessed_object = noone;
current_vent = noone;
action_object = noone;
action_radius = 30;

dash_distance = 32*2.5;
dash_speed = 800;
action_cooldown = 0.5*room_speed;

#region smoke
smoke_part = part_type_create();
part_type_shape(smoke_part, pt_shape_disk);
part_type_size(smoke_part, .05, .10, -.001, 0);
part_type_color1(smoke_part,$544646);
part_type_color2(smoke_part,$544646,$1E1717);
part_type_life(smoke_part, 60, 100);

smoke_sys = part_system_create();
part_system_position(smoke_sys, 0, 0);
part_system_depth(smoke_sys, 500000);
part_system_automatic_draw(smoke_sys, true);

em_diam = 8;
smoke_em = part_emitter_create(smoke_sys);
part_emitter_region(smoke_sys, smoke_em, x-em_diam, x+em_diam, y-em_diam, y+em_diam, ps_shape_ellipse, ps_distr_gaussian);

part_emitter_stream(smoke_sys, smoke_em, smoke_part, 0);
#endregion

#region possession particles
pos_part = part_type_create();
part_type_sprite(pos_part, spr_possession_spark, 0, 0, 0);
part_type_life(pos_part, 20, 40);
part_type_gravity(pos_part, .07, 90);

pos_sys = part_system_create();
part_system_position(pos_sys, 0, 0);
part_system_depth(pos_sys, 200000);
part_system_automatic_draw(pos_sys, true);

pos_em = part_emitter_create(pos_sys);
#endregion

with instance_create_layer(x, y, "Entities", dash_box) {
	attached_to = other;
	other.dash_hitbox = self;
}

#region state machine setup
state_machine_init();
state_create("Default", player_default);
state_create("Dash", player_dash);
state_create_entry("Dash", player_dash_entry);
state_create_exit("Dash", player_dash_exit);
state_create("Possessing", player_possessing);
state_create("Vent", player_vent);
state_create_entry("Vent", player_vent_entry);
state_create_exit("Vent", player_vent_exit);
state_create("VentDash", player_vent_dash);
state_create_entry("VentDash", player_vent_dash_entry);
state_create_exit("VentDash", player_vent_dash_exit);
state_create("Dead", player_dead);

state_init("Default");
#endregion
