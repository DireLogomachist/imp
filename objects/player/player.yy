{
    "id": "ec8cea41-f490-4294-aaf8-05f02f04b015",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "player",
    "eventList": [
        {
            "id": "0500f38c-ae69-455c-b182-cd0d4689309b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ec8cea41-f490-4294-aaf8-05f02f04b015"
        },
        {
            "id": "d9ff3b0b-5f04-4a45-91d4-9f72ad297d4b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ec8cea41-f490-4294-aaf8-05f02f04b015"
        },
        {
            "id": "edf257bc-91a2-4181-b46b-075e102de4c5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "ec8cea41-f490-4294-aaf8-05f02f04b015"
        },
        {
            "id": "190b67b3-81d4-4634-b7df-acfd38f5092c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "ec8cea41-f490-4294-aaf8-05f02f04b015"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bd272022-b402-472f-bb65-a842201d949a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "541dd16e-e046-4976-8a50-5a48bb76b2bc",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "cad3fd2f-db38-4dea-91a8-d838bb222f20",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 0
        },
        {
            "id": "063b5889-b036-4277-bfe8-db873e7398df",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 32,
            "y": 32
        },
        {
            "id": "25985df1-f97b-4259-89b9-28298098c25b",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 32
        }
    ],
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": true,
    "spriteId": "289affc6-16ad-4878-b509-e61eb9acbf39",
    "visible": true
}