///@description player mechanics


if !player_alive return;

part_system_depth(smoke_sys, self.depth+100);
part_emitter_region(smoke_sys, smoke_em, x-em_diam, x+em_diam, y-em_diam, y+em_diam, ps_shape_ellipse, ps_distr_linear);

event_inherited();
state_execute();

if keyboard_check_pressed(vk_backspace) { player_death(); }
