{
    "id": "08172e97-7622-45d0-8237-2c24c13acf8b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "pathfinder",
    "eventList": [
        {
            "id": "eacd578c-0eb2-4451-abfc-6314c4e57216",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08172e97-7622-45d0-8237-2c24c13acf8b"
        },
        {
            "id": "808bb585-717c-499e-9a2e-b54ca1820bd3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08172e97-7622-45d0-8237-2c24c13acf8b"
        },
        {
            "id": "a18c9e43-d257-4a1a-aa66-74c14afc9446",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08172e97-7622-45d0-8237-2c24c13acf8b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cad37f6f-d083-4233-89fe-f93ff1e6c720",
    "visible": true
}