/// @description 


if mouse_check_button_pressed(mb_left) {
	var mx = (mouse_x div 32)*32;
	var my = (mouse_y div 32)*32;
	
	path_end();
	var old_path = path;
	path = path_add();
	if old_path != noone
		path_delete(old_path);
	if mp_grid_path(global.path_grid, path, x, y, mx, my, 0) {
		path_start(path, 600*delta_time/1000000, path_action_stop, false);
		show_debug_message("Going to destination: " + string(mx) + ":" + string(my));
	} else {
		show_debug_message("No path found");
	}
}
