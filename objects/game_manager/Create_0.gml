/// @description


// Game settings and controls
global.gm = self;

global.camera_w = 480;
global.camera_h = 270;
global.window_w = 960;
global.window_h = 540;

global.dash_input = vk_space;
global.action_input = ord("A");
global.pause_input = vk_escape;

global.flags = ds_map_create();
global.flags[? "menu_started"] = false;

lvl_index = 0;
levels[0] = start;
levels[1] = intro;
levels[2] = laser;
levels[3] = guards;
levels[4] = move_fast;
levels[5] = circles;
levels[6] = foyer;
levels[7] = gallery;

//levels[5] = pipes;
//levels[6] = test_room;
//levels[7] = test_room_2;
//levels[8] = test_room_3;
//levels[9] = test_room_4;
//levels[10] = test_pathfinding;

levels[array_length_1d(levels)] = penthouse;
levels[array_length_1d(levels)] = ending;

lvl_index = 0;
room_goto(levels[lvl_index]);
