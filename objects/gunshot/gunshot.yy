{
    "id": "34cea453-3978-4986-99c3-7fccd7df47f8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gunshot",
    "eventList": [
        {
            "id": "ee2db8c8-8f70-48c9-baf4-fe7722811bd9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34cea453-3978-4986-99c3-7fccd7df47f8"
        },
        {
            "id": "0dcfe050-bdcd-4c24-8ead-69fb56d7c11d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "34cea453-3978-4986-99c3-7fccd7df47f8"
        },
        {
            "id": "4d1485e2-3f3c-418c-b71d-e809735276ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "34cea453-3978-4986-99c3-7fccd7df47f8"
        },
        {
            "id": "5754ec02-e343-4960-851f-e6fd230eae4c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "34cea453-3978-4986-99c3-7fccd7df47f8"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}