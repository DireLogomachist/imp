/// @description draw gunshot line


draw_line_width_color(x1, y1, x2, y2, 2, c_yellow, c_yellow);
draw_line_width_color(x1, y1, x2, y2, 1, c_white, c_white);
