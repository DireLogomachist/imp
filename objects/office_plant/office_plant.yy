{
    "id": "3129bf51-ecdd-4165-87fd-67b7d4b8031d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "office_plant",
    "eventList": [
        {
            "id": "6f963123-4a90-47af-803f-22a1a9aba970",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3129bf51-ecdd-4165-87fd-67b7d4b8031d"
        },
        {
            "id": "f87f69d8-ee55-4b22-a51f-e9da612b62c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3129bf51-ecdd-4165-87fd-67b7d4b8031d"
        },
        {
            "id": "d712d3f2-3ef4-4df6-bbdc-372f0b395da3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3129bf51-ecdd-4165-87fd-67b7d4b8031d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "da97011b-3be2-49ff-9d64-016183312b04",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "73bedfad-936d-4b09-88b2-7ae0e260ac1d",
    "visible": true
}