{
    "id": "12af242f-5b05-42fa-b646-84f425460348",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "newspaper_1",
    "eventList": [
        
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "51d0291b-849b-4fa7-8f97-28357c190642",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": true,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "64851bcb-8485-4527-bc93-830b73310969",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "7015e5c2-c9b2-4a52-9360-9f3cf00b94a7",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 208,
            "y": 0
        },
        {
            "id": "021c7706-a34c-4bce-8c29-9780791e8dd5",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 208,
            "y": 156
        },
        {
            "id": "e9bcb7a2-6709-4e04-8c11-b71441a2a169",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 156
        }
    ],
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3ad1c185-4a01-4ae2-8119-41bc42d1359a",
    "visible": true
}