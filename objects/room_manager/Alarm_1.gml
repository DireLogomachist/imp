/// @description exit to menu


with global.gm {
	audio_stop_sound(au_room_background);
	audio_stop_sound(au_vent_background);
	audio_stop_sound(au_lobby_entrance);
	lvl_index = 0;
	room_goto(levels[lvl_index]);
}
