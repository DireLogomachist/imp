/// @description 


global.rm_manager = self;

player_death_wait = 2*room_speed;

level_gen_seed = 12312312;
random_set_seed(level_gen_seed);
randomized = false;

//button_a = instance_create_layer(0, 0, "GUI", gui_button_a);
//button_space = instance_create_layer(0, 0, "GUI", gui_button_space);

#region layers and shaders
dark_layers = array("VFX", "Elevators", "Entities", "Walls", "Splatter", "Deco", "Floor", "Art");
walls = layer_tilemap_get_id("Walls");
duct_layer = layer_get_id("Ducts");

shader_set(duct_shader);
global.duct_alpha = shader_get_uniform(duct_shader, "f_duct_alpha");
shader_set_uniform_f(global.duct_alpha, 0.0);
shader_reset();

layer_shader(duct_layer, duct_shader);
#endregion

global.actionable_instances = array_create(0);
for(var i = 0; i < instance_number(actionable); i++) {
	var inst = instance_find(actionable, i);
	global.actionable_instances[i] = inst;
}

if room == intro and global.flags[? "menu_started"] == true {
	global.flags[?"menu_started"] = false;
	instance_create_depth(0,0,0,fade_in_slow_controller);
	audio_play_sound(au_lobby_entrance, 0, false);
} else {
	instance_create_depth(0,0,0,fade_in_controller);
}

audio_play_sound(au_room_background, 0, true);

#region debug draw settings
draw_set_color(c_lime);
draw_set_font(fn_default);

debug_player_state = false;
debug_actionable_dist = false;
debug_real_fps = false;
#endregion

#region AI pathfinding
global.path_grid = mp_grid_create(0, 0, room_width div 32, room_height div 32, 32, 32);

var wall_map = layer_tilemap_get_id(layer_get_id("Walls"));
for(var i=0; i<(room_width div 32); i++) {
	for(var j=0; j<(room_height div 32); j++) {
		if (tilemap_get(wall_map, i, j) > 0) {
			mp_grid_add_cell(global.path_grid, i, j);
		}
	}
}
#endregion
