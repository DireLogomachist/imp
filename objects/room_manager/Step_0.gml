/// @description

shader_set_uniform_f(global.duct_alpha, 1.0);
#region Window Sizing
window_set_size(global.window_w, global.window_h);
#endregion

#region Controls
if keyboard_check_pressed(vk_escape) {
	instance_create_depth(0,0,0,fade_out_controller);
	alarm[1] = 2.0*room_speed;
}
#endregion

if !randomized {
	randomize();
	randomized = true;
}
