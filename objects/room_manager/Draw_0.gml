/// @description


var view_x = camera_get_view_x(camera_get_active());
var view_y = camera_get_view_y(camera_get_active());


if global.player_inst != noone {
	if debug_player_state
		draw_text(view_x + 10, view_y + 5, global.player_inst.state_name);
	
	if debug_actionable_dist {
		for(var i = 0; i < array_length_1d(global.actionable_instances); i++) {
			var a = global.actionable_instances[i]
			if point_distance(global.player_inst.x, global.player_inst.y, a.x + a.offset_x, a.y + a.offset_y) <= global.player_inst.action_radius {
				draw_line_color(global.player_inst.x,global.player_inst.y, a.x + a.offset_x, a.y + a.offset_y,c_red, c_red);
			} else {
				draw_line(global.player_inst.x,global.player_inst.y, a.x + a.offset_x, a.y + a.offset_y);
			}
		}
	}
}
 
#region Camera Tracking
	var cam_x = global.player_inst.x - camera_get_view_width(camera_get_active())/2;
	var cam_y = global.player_inst.y - camera_get_view_height(camera_get_active())/2;
	camera_set_view_pos(camera_get_active(), cam_x, cam_y);
#endregion

if debug_real_fps
	draw_text(view_x + global.camera_w - 100, view_y + global.camera_h - 20, "FPS:" + string(fps_real));
