{
    "id": "96e43c85-46c2-4f33-8276-2d74d21f740e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "room_manager",
    "eventList": [
        {
            "id": "a7df37ce-7f24-4d7d-a7b0-1725969084fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96e43c85-46c2-4f33-8276-2d74d21f740e"
        },
        {
            "id": "6fc9a21d-9b97-4d9b-9565-67ac4a8597e9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "96e43c85-46c2-4f33-8276-2d74d21f740e"
        },
        {
            "id": "23bebff0-e04f-4691-ad81-73c1d63ca631",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "96e43c85-46c2-4f33-8276-2d74d21f740e"
        },
        {
            "id": "2eefc105-3fa0-4613-94f9-de1c6339829f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "96e43c85-46c2-4f33-8276-2d74d21f740e"
        },
        {
            "id": "8bd37143-aa35-41d5-8adc-366fba7b46f7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "96e43c85-46c2-4f33-8276-2d74d21f740e"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}