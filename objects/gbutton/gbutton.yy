{
    "id": "b99b105c-74eb-428c-91b0-e33c41ec1c62",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "gbutton",
    "eventList": [
        {
            "id": "4723a824-b4f7-4f83-8ab8-3515c646c4fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b99b105c-74eb-428c-91b0-e33c41ec1c62"
        },
        {
            "id": "e4478121-c867-4a4c-a849-a8d41162a8c8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b99b105c-74eb-428c-91b0-e33c41ec1c62"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "58cff814-8072-47e7-9ad8-15d6e1536d8b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "2d354c93-9659-4257-a1a3-9a7445de5fc4",
    "visible": true
}