{
    "id": "1b10b5ac-1d0b-40af-bdce-39584ffd151d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "fade_in_slow_controller",
    "eventList": [
        {
            "id": "846c1c93-6b49-41e5-a536-811353710eae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1b10b5ac-1d0b-40af-bdce-39584ffd151d"
        },
        {
            "id": "a90fda62-eeea-40dc-b0de-5c48bc88fbba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "1b10b5ac-1d0b-40af-bdce-39584ffd151d"
        },
        {
            "id": "c2d388a5-c6da-42ce-b79e-2895ec929843",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "1b10b5ac-1d0b-40af-bdce-39584ffd151d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "43e5d18e-b08c-43a5-93c2-2e28a2092e8e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}