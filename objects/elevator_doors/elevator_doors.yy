{
    "id": "bbe8d600-cbea-4571-ab40-1d5bd434b213",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "elevator_doors",
    "eventList": [
        {
            "id": "c6c2977c-b74a-401c-b2ec-cd2d83ea72d5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbe8d600-cbea-4571-ab40-1d5bd434b213"
        },
        {
            "id": "6b166247-1d76-4301-8339-6dc31d8e0072",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bbe8d600-cbea-4571-ab40-1d5bd434b213"
        },
        {
            "id": "5e8dda76-6fed-43ae-a8f1-0d35a4f79fb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bbe8d600-cbea-4571-ab40-1d5bd434b213"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
    "visible": true
}