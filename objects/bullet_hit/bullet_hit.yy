{
    "id": "63e4257f-43a8-4252-b733-bd20d3c269ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "bullet_hit",
    "eventList": [
        {
            "id": "0a340167-5374-4789-96ad-514b40daaeb5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "63e4257f-43a8-4252-b733-bd20d3c269ba"
        },
        {
            "id": "b93909b2-45b3-4040-b4e1-1d852d5573de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "63e4257f-43a8-4252-b733-bd20d3c269ba"
        },
        {
            "id": "cf1b9a94-a61d-40d5-8bec-bedff4aae06d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "63e4257f-43a8-4252-b733-bd20d3c269ba"
        }
    ],
    "maskSpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5c3da86f-ced9-4191-a410-379ce0544daa",
    "visible": true
}