{
    "id": "0f5e664d-31c0-4960-ae36-131f4cab6956",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "laser_r",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": [
        {
            "id": "2c2be750-8d85-4a5a-9118-389dcce7f5a5",
            "modelName": "GMOverriddenProperty",
            "mvc": "1.0",
            "objectId": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7",
            "propertyId": "b4236acc-a8f2-4530-a314-81cee7037bb0",
            "value": "180"
        }
    ],
    "parentObjectId": "ddf77e72-3deb-4af4-a440-6c57ccbf81a7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": [
        
    ],
    "solid": false,
    "spriteId": "a1d50610-ed33-4acd-a443-43d17857084e",
    "visible": true
}