{
    "id": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guard",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 6,
    "bbox_right": 23,
    "bbox_top": 17,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4fa3832-857c-4397-a3f8-9604a6e5f288",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
            "compositeImage": {
                "id": "df33e559-1ea0-4459-9a31-9dc7d1af70da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4fa3832-857c-4397-a3f8-9604a6e5f288",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c8f8c28-87b8-4610-9c94-6df7ea426c18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4fa3832-857c-4397-a3f8-9604a6e5f288",
                    "LayerId": "6fe72b0e-dd2a-465c-afde-a21222e06a94"
                }
            ]
        },
        {
            "id": "3e9145a8-2ba7-4c8a-98a6-80c5bfab5f1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
            "compositeImage": {
                "id": "eed35641-524d-417e-8477-b98c80dd7c83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3e9145a8-2ba7-4c8a-98a6-80c5bfab5f1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "690b7493-6425-487b-a1b0-14f813ac4ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3e9145a8-2ba7-4c8a-98a6-80c5bfab5f1a",
                    "LayerId": "6fe72b0e-dd2a-465c-afde-a21222e06a94"
                }
            ]
        },
        {
            "id": "5aa70d7c-0c63-4bf1-a7f4-d2efcddeb042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
            "compositeImage": {
                "id": "ef1eb4f2-bcb0-4cfb-9baf-223b29fc044d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5aa70d7c-0c63-4bf1-a7f4-d2efcddeb042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "912818f7-0a22-4185-85bc-27f06451a45e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5aa70d7c-0c63-4bf1-a7f4-d2efcddeb042",
                    "LayerId": "6fe72b0e-dd2a-465c-afde-a21222e06a94"
                }
            ]
        },
        {
            "id": "26ea2747-10f1-4c88-a5b0-2c6228447ade",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
            "compositeImage": {
                "id": "fd598a63-bc17-4620-8b8c-26668c9d443e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26ea2747-10f1-4c88-a5b0-2c6228447ade",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83e3eff3-ffee-4b9a-a44d-0003d99aecf4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26ea2747-10f1-4c88-a5b0-2c6228447ade",
                    "LayerId": "6fe72b0e-dd2a-465c-afde-a21222e06a94"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6fe72b0e-dd2a-465c-afde-a21222e06a94",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5276ea22-e4e5-4b0c-b315-8cb5ff31259f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 20
}