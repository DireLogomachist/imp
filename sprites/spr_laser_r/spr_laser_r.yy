{
    "id": "a1d50610-ed33-4acd-a443-43d17857084e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_r",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 13,
    "bbox_right": 15,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "384d48de-650a-4ed7-b4f9-8c6cc9972543",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a1d50610-ed33-4acd-a443-43d17857084e",
            "compositeImage": {
                "id": "d2c2212f-4061-430f-bd2f-e9b11451522b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "384d48de-650a-4ed7-b4f9-8c6cc9972543",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe4d0c7e-528d-4b47-a1cc-0d7fce356c10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "384d48de-650a-4ed7-b4f9-8c6cc9972543",
                    "LayerId": "8e38ba63-7259-4b05-9af0-f1458307efd4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e38ba63-7259-4b05-9af0-f1458307efd4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a1d50610-ed33-4acd-a443-43d17857084e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 14,
    "yorig": 7
}