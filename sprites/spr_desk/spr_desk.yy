{
    "id": "0c99145c-3e2a-4860-a425-7377ad2dd56a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_desk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 7,
    "bbox_right": 89,
    "bbox_top": 11,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "186779dc-f8b3-471c-8b76-5ef73fcb3b4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c99145c-3e2a-4860-a425-7377ad2dd56a",
            "compositeImage": {
                "id": "7ae5500a-5a90-4757-a89e-a50c613180fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "186779dc-f8b3-471c-8b76-5ef73fcb3b4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a543cfd0-d58d-44b8-923c-e29428ef20db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "186779dc-f8b3-471c-8b76-5ef73fcb3b4a",
                    "LayerId": "f39db336-9011-4dcd-a363-dddda4d971fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f39db336-9011-4dcd-a363-dddda4d971fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c99145c-3e2a-4860-a425-7377ad2dd56a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 16
}