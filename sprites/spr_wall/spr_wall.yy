{
    "id": "84605899-91e2-4bc1-a4c0-bdef74c1559a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "594c388a-83d0-4812-b374-b62a444d8c9f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "84605899-91e2-4bc1-a4c0-bdef74c1559a",
            "compositeImage": {
                "id": "e0dd0132-adcc-4594-874b-5cfa83ef8f6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "594c388a-83d0-4812-b374-b62a444d8c9f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2f82b801-8331-4073-ab65-a1d9ba1728d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "594c388a-83d0-4812-b374-b62a444d8c9f",
                    "LayerId": "1bee8983-beeb-4419-b1b2-ab0f2fc4df6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1bee8983-beeb-4419-b1b2-ab0f2fc4df6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "84605899-91e2-4bc1-a4c0-bdef74c1559a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}