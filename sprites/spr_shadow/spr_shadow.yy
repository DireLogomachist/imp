{
    "id": "695949e2-72e4-49b4-bcc4-90ff13a0365c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 10,
    "bbox_right": 22,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e188271-b293-496c-b714-b62b54f7f236",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "695949e2-72e4-49b4-bcc4-90ff13a0365c",
            "compositeImage": {
                "id": "75ed33be-b1e7-4742-8e22-99f9f867a885",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e188271-b293-496c-b714-b62b54f7f236",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd234de8-9212-4e99-9d19-ca7d5cd57381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e188271-b293-496c-b714-b62b54f7f236",
                    "LayerId": "a2f8362a-301e-4574-97ca-42e8a8e6f22e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a2f8362a-301e-4574-97ca-42e8a8e6f22e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "695949e2-72e4-49b4-bcc4-90ff13a0365c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 19
}