{
    "id": "d868b703-5582-464d-b120-800e1352bab9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_possession_spark",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 9,
    "bbox_left": 5,
    "bbox_right": 9,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2c0c4a5f-735b-437f-bd0b-2cb9f3d6e77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d868b703-5582-464d-b120-800e1352bab9",
            "compositeImage": {
                "id": "1129718c-f0fd-42fb-b2a9-4c85fc3a24df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2c0c4a5f-735b-437f-bd0b-2cb9f3d6e77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "594043b9-eb18-4a64-a948-7e14ab6da515",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2c0c4a5f-735b-437f-bd0b-2cb9f3d6e77d",
                    "LayerId": "b7aef229-bc21-4f12-8d3e-cfc36b8fad8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b7aef229-bc21-4f12-8d3e-cfc36b8fad8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d868b703-5582-464d-b120-800e1352bab9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}