{
    "id": "289affc6-16ad-4878-b509-e61eb9acbf39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_imp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea4160ff-e9ea-4a5e-ab79-63167e5919f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "289affc6-16ad-4878-b509-e61eb9acbf39",
            "compositeImage": {
                "id": "103e71dd-b5c0-4dd0-84b4-e8c606bd5bd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea4160ff-e9ea-4a5e-ab79-63167e5919f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "880aad97-3eaa-4e0d-bb80-eaaceb3aacee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea4160ff-e9ea-4a5e-ab79-63167e5919f9",
                    "LayerId": "f866eb65-d5a0-40ef-b49f-a9955767089e"
                }
            ]
        },
        {
            "id": "40125862-265f-47d6-9780-57155f734f5f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "289affc6-16ad-4878-b509-e61eb9acbf39",
            "compositeImage": {
                "id": "8087ed81-0a27-4436-9183-9aeab02972ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40125862-265f-47d6-9780-57155f734f5f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cd7f952-4eae-40d1-a310-046fbeb229d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40125862-265f-47d6-9780-57155f734f5f",
                    "LayerId": "f866eb65-d5a0-40ef-b49f-a9955767089e"
                }
            ]
        },
        {
            "id": "b6318f07-83f3-435c-94d6-6caa6b2ba039",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "289affc6-16ad-4878-b509-e61eb9acbf39",
            "compositeImage": {
                "id": "34b1f234-ded9-4bc5-8def-da6fc692067b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b6318f07-83f3-435c-94d6-6caa6b2ba039",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56b63baa-90a9-4244-ac48-26ec3d8b2c4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b6318f07-83f3-435c-94d6-6caa6b2ba039",
                    "LayerId": "f866eb65-d5a0-40ef-b49f-a9955767089e"
                }
            ]
        },
        {
            "id": "8b312df8-8864-4329-85d9-323cbb74e1ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "289affc6-16ad-4878-b509-e61eb9acbf39",
            "compositeImage": {
                "id": "5c699ffa-2663-49e7-aec1-985bb71d153e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b312df8-8864-4329-85d9-323cbb74e1ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0fd7090-a169-4ce2-9a18-8dcbec1c5ab8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b312df8-8864-4329-85d9-323cbb74e1ee",
                    "LayerId": "f866eb65-d5a0-40ef-b49f-a9955767089e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f866eb65-d5a0-40ef-b49f-a9955767089e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "289affc6-16ad-4878-b509-e61eb9acbf39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 20
}