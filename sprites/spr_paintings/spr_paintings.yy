{
    "id": "945f5b06-310d-4751-b802-f6d6cad7f9de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_paintings",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 56,
    "bbox_left": 3,
    "bbox_right": 92,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "73660a87-e65c-41a5-ba7a-f2c578213004",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "945f5b06-310d-4751-b802-f6d6cad7f9de",
            "compositeImage": {
                "id": "641dbf71-7cc9-4120-beee-380e128ef2fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73660a87-e65c-41a5-ba7a-f2c578213004",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12ca35fa-3b89-48cc-953e-b5fe50b73b24",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73660a87-e65c-41a5-ba7a-f2c578213004",
                    "LayerId": "68891c45-4a37-41ba-889b-0f774224bc8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "68891c45-4a37-41ba-889b-0f774224bc8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "945f5b06-310d-4751-b802-f6d6cad7f9de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}