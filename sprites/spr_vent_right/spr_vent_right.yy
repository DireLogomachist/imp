{
    "id": "eb72618a-505f-4142-afcc-3ed89091d36a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vent_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 27,
    "bbox_right": 31,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57274f6b-3da2-4c04-841c-15fc9ac62ec5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb72618a-505f-4142-afcc-3ed89091d36a",
            "compositeImage": {
                "id": "4b2a41b2-99f6-4400-8dbf-94438afb404c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57274f6b-3da2-4c04-841c-15fc9ac62ec5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "495996ab-5fb5-4144-85f7-b70ef77be493",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57274f6b-3da2-4c04-841c-15fc9ac62ec5",
                    "LayerId": "c1aa80fc-84b8-4390-84b1-0bcac0f19c76"
                }
            ]
        },
        {
            "id": "c19c3f43-6ec9-4eed-b20c-6572b19be347",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb72618a-505f-4142-afcc-3ed89091d36a",
            "compositeImage": {
                "id": "250c4305-0e4b-4005-9ff3-ec2fb54cc884",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c19c3f43-6ec9-4eed-b20c-6572b19be347",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47b98aa0-5005-4fd1-8a63-2d749f41443b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c19c3f43-6ec9-4eed-b20c-6572b19be347",
                    "LayerId": "c1aa80fc-84b8-4390-84b1-0bcac0f19c76"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c1aa80fc-84b8-4390-84b1-0bcac0f19c76",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb72618a-505f-4142-afcc-3ed89091d36a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": -3,
    "yorig": 0
}