{
    "id": "c94dc038-d6a4-4cf8-bdb7-fb1a88a0cf53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_button_space",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 8,
    "bbox_right": 54,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7358a72a-fa92-4d8f-85a6-b0bfc92fcdf7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c94dc038-d6a4-4cf8-bdb7-fb1a88a0cf53",
            "compositeImage": {
                "id": "76ac8d87-36ab-40dd-b616-2b1fe9783299",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7358a72a-fa92-4d8f-85a6-b0bfc92fcdf7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a05132d-a973-4e48-871a-cadee77649bd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7358a72a-fa92-4d8f-85a6-b0bfc92fcdf7",
                    "LayerId": "6049a5a7-d84b-41b4-93df-d1a24b5e63c6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6049a5a7-d84b-41b4-93df-d1a24b5e63c6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c94dc038-d6a4-4cf8-bdb7-fb1a88a0cf53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}