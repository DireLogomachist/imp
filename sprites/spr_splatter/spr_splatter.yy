{
    "id": "e93124d4-c0d5-4f5d-a1b6-b5fc89c690bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_splatter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 5,
    "bbox_right": 58,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f1219cf7-45de-4276-a74f-e864f498e69d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e93124d4-c0d5-4f5d-a1b6-b5fc89c690bf",
            "compositeImage": {
                "id": "ba7ad861-3983-4fff-ac8b-572b187f30b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1219cf7-45de-4276-a74f-e864f498e69d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78bdd56b-f29e-4e81-ad07-92391d6464fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1219cf7-45de-4276-a74f-e864f498e69d",
                    "LayerId": "3dc7032d-0c59-4122-892e-5d57af987aac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3dc7032d-0c59-4122-892e-5d57af987aac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e93124d4-c0d5-4f5d-a1b6-b5fc89c690bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 24
}