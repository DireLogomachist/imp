{
    "id": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_empty",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "50b1273b-12e6-4346-a209-6d63a255d4e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
            "compositeImage": {
                "id": "106771ec-ea31-44c4-a702-81845eaa4484",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "50b1273b-12e6-4346-a209-6d63a255d4e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "079d0ad7-3de6-4c17-92e2-e182031c9551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "50b1273b-12e6-4346-a209-6d63a255d4e8",
                    "LayerId": "299d369b-fb8a-4f67-af62-30672230f4f2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "299d369b-fb8a-4f67-af62-30672230f4f2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d8d4076e-a876-414e-94a4-4a0b6b6626ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}