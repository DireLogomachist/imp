{
    "id": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dust",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 3,
    "bbox_right": 13,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fb665b8-a2e7-44e6-84c8-4cb64ff6467e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "compositeImage": {
                "id": "130e34e4-a6e7-4c6a-87ed-c97e4a22ceb0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb665b8-a2e7-44e6-84c8-4cb64ff6467e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "933e2993-d41b-4521-90e4-e30cf5325b64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb665b8-a2e7-44e6-84c8-4cb64ff6467e",
                    "LayerId": "40d97add-4c9f-41c3-9a61-95666422295b"
                }
            ]
        },
        {
            "id": "f8908dba-7fee-4ebf-94d5-276db3532e98",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "compositeImage": {
                "id": "eab1960a-75f2-4be4-be11-e00a1c4dd6a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8908dba-7fee-4ebf-94d5-276db3532e98",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a1438ad-7711-4d5f-adfa-d5ab07330f99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8908dba-7fee-4ebf-94d5-276db3532e98",
                    "LayerId": "40d97add-4c9f-41c3-9a61-95666422295b"
                }
            ]
        },
        {
            "id": "d1de7b85-cbdd-4d72-9390-7e7033cfe9e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "compositeImage": {
                "id": "e353bea7-a235-42d2-987a-09abe0ea1bda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1de7b85-cbdd-4d72-9390-7e7033cfe9e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "11d80819-b54b-4c29-9ca1-117a8a0b0552",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1de7b85-cbdd-4d72-9390-7e7033cfe9e0",
                    "LayerId": "40d97add-4c9f-41c3-9a61-95666422295b"
                }
            ]
        },
        {
            "id": "def83513-4634-41eb-94d0-2459a776f35f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "compositeImage": {
                "id": "90f4a618-b75e-4683-aef3-8d70ed1e8d08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "def83513-4634-41eb-94d0-2459a776f35f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "876651bf-b44b-4ef2-b0d7-906b588b1c23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "def83513-4634-41eb-94d0-2459a776f35f",
                    "LayerId": "40d97add-4c9f-41c3-9a61-95666422295b"
                }
            ]
        },
        {
            "id": "c44a2821-f7a3-4bbe-961c-340e0cf8bd61",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "compositeImage": {
                "id": "ecb67396-d507-4202-b7a7-798898500197",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c44a2821-f7a3-4bbe-961c-340e0cf8bd61",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c88dc00e-db2e-494a-88fa-27c320ef61fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c44a2821-f7a3-4bbe-961c-340e0cf8bd61",
                    "LayerId": "40d97add-4c9f-41c3-9a61-95666422295b"
                }
            ]
        },
        {
            "id": "1b284f98-6afb-46f5-b58a-f4a0696ce05b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "compositeImage": {
                "id": "2ae3ac44-6491-4563-9ae1-09170049ff39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b284f98-6afb-46f5-b58a-f4a0696ce05b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ec1e81-c8a4-4813-a010-ad9a80e406e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b284f98-6afb-46f5-b58a-f4a0696ce05b",
                    "LayerId": "40d97add-4c9f-41c3-9a61-95666422295b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "40d97add-4c9f-41c3-9a61-95666422295b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2baaea20-92e6-4b75-8216-dfdd7bc996b2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}