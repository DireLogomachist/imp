{
    "id": "0d282d1a-727e-477a-b151-05ab19a682b8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vent_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2220da14-b31f-4348-8e4d-a583e6a36aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d282d1a-727e-477a-b151-05ab19a682b8",
            "compositeImage": {
                "id": "1ebe0ec4-be56-493e-a4ea-ad43049f10a2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2220da14-b31f-4348-8e4d-a583e6a36aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82bfdac4-7ea7-45b5-ab76-719d13fea8fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2220da14-b31f-4348-8e4d-a583e6a36aa9",
                    "LayerId": "0d4ca8fa-1cb6-488a-be6c-307ab11f320f"
                }
            ]
        },
        {
            "id": "7ce47bed-bead-4f1a-9ef8-186a82c8a038",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d282d1a-727e-477a-b151-05ab19a682b8",
            "compositeImage": {
                "id": "df6017a5-56da-4b20-9a10-c8f0719aae54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ce47bed-bead-4f1a-9ef8-186a82c8a038",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3e141cd5-59db-4243-98da-b9835175c7f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ce47bed-bead-4f1a-9ef8-186a82c8a038",
                    "LayerId": "0d4ca8fa-1cb6-488a-be6c-307ab11f320f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0d4ca8fa-1cb6-488a-be6c-307ab11f320f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d282d1a-727e-477a-b151-05ab19a682b8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 3,
    "yorig": 0
}