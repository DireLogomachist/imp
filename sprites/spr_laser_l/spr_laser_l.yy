{
    "id": "f1e36853-b37e-4bdd-9a5f-9f9c8e77ca30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_l",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 2,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "22ea3aff-b6ec-4906-93ef-5abcd6524cac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1e36853-b37e-4bdd-9a5f-9f9c8e77ca30",
            "compositeImage": {
                "id": "0f53a702-d583-44bd-a8e6-ff072ba801a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22ea3aff-b6ec-4906-93ef-5abcd6524cac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe7a00af-4cff-47fb-b0fe-d36456d6cd71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22ea3aff-b6ec-4906-93ef-5abcd6524cac",
                    "LayerId": "c1a1acb1-0bfb-4d6d-8452-5863d8182ccb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "c1a1acb1-0bfb-4d6d-8452-5863d8182ccb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1e36853-b37e-4bdd-9a5f-9f9c8e77ca30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 2,
    "yorig": 7
}