{
    "id": "031ce5ae-d61d-4c14-96b2-182c8bff6c30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rug",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 152,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "acac8581-a695-40f3-8da7-e05322fee7b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "031ce5ae-d61d-4c14-96b2-182c8bff6c30",
            "compositeImage": {
                "id": "36e59734-7e6d-499c-99f0-977e221b0bbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acac8581-a695-40f3-8da7-e05322fee7b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3bce5983-2258-45a1-9b2c-8222b15f195f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acac8581-a695-40f3-8da7-e05322fee7b1",
                    "LayerId": "77bf4f65-1625-498c-a837-c048f467a2ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 160,
    "layers": [
        {
            "id": "77bf4f65-1625-498c-a837-c048f467a2ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "031ce5ae-d61d-4c14-96b2-182c8bff6c30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}