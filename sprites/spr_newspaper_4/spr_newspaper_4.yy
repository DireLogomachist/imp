{
    "id": "3e02ae76-f819-4be1-bdc2-69f6b404c7fd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newspaper_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98f8bb61-0b98-4aaa-a21d-ee063d17c424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e02ae76-f819-4be1-bdc2-69f6b404c7fd",
            "compositeImage": {
                "id": "dda0b643-a91d-4fd9-9aef-17fa8e964e75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98f8bb61-0b98-4aaa-a21d-ee063d17c424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2942c80-7362-4ad8-af04-b52e97847d93",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98f8bb61-0b98-4aaa-a21d-ee063d17c424",
                    "LayerId": "7e532c3a-886d-4d21-8c4c-6aef9cd8e6c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "7e532c3a-886d-4d21-8c4c-6aef9cd8e6c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e02ae76-f819-4be1-bdc2-69f6b404c7fd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 220,
    "xorig": 0,
    "yorig": 0
}