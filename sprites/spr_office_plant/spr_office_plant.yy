{
    "id": "73bedfad-936d-4b09-88b2-7ae0e260ac1d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_office_plant",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 6,
    "bbox_right": 26,
    "bbox_top": 50,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8cdd0878-da14-42c9-a990-e3502a7f73c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73bedfad-936d-4b09-88b2-7ae0e260ac1d",
            "compositeImage": {
                "id": "2594ace8-296d-4976-9887-9ae09e47ff4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cdd0878-da14-42c9-a990-e3502a7f73c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "293907c1-6c30-4cc9-b0c9-284048ddf371",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cdd0878-da14-42c9-a990-e3502a7f73c4",
                    "LayerId": "7e088982-3f14-47de-b167-c0975f15be56"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7e088982-3f14-47de-b167-c0975f15be56",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73bedfad-936d-4b09-88b2-7ae0e260ac1d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 49
}