{
    "id": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_elevator_doors",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "69950b1f-7be1-4b84-9c9d-7e894560f999",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "compositeImage": {
                "id": "19c91e6b-aac8-4823-bcd2-a1ae79cc207d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69950b1f-7be1-4b84-9c9d-7e894560f999",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "133fee6b-63de-4584-9610-89b30f9b7215",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69950b1f-7be1-4b84-9c9d-7e894560f999",
                    "LayerId": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc"
                }
            ]
        },
        {
            "id": "2918507c-a84d-4611-b6d2-856680fac15d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "compositeImage": {
                "id": "118efaf8-b0b4-4b8b-b260-dd55c9f91e6e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2918507c-a84d-4611-b6d2-856680fac15d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a96ac08-ad99-4265-a575-e9abbebbddec",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2918507c-a84d-4611-b6d2-856680fac15d",
                    "LayerId": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc"
                }
            ]
        },
        {
            "id": "9ac5eb12-4ff3-427b-a142-0f51fa3ba2ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "compositeImage": {
                "id": "59df1393-645a-4c43-90dd-00111c235f95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ac5eb12-4ff3-427b-a142-0f51fa3ba2ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aec23ea2-dbf2-4de0-a73f-5a4c7f68e07f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ac5eb12-4ff3-427b-a142-0f51fa3ba2ea",
                    "LayerId": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc"
                }
            ]
        },
        {
            "id": "2f778a3a-6ef7-4375-94aa-a8819de94a44",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "compositeImage": {
                "id": "c299d248-b563-4508-9aab-05645b067fac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f778a3a-6ef7-4375-94aa-a8819de94a44",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2d0b1ad-73a5-4e4f-a843-b04650338d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f778a3a-6ef7-4375-94aa-a8819de94a44",
                    "LayerId": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc"
                }
            ]
        },
        {
            "id": "cd1c875e-ca43-40b4-adad-c4d10f2aaa47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "compositeImage": {
                "id": "1a6c7686-edff-4104-86e0-a1f19b29b088",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1c875e-ca43-40b4-adad-c4d10f2aaa47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1f39c104-3797-4abb-89bf-1c3929474dc3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1c875e-ca43-40b4-adad-c4d10f2aaa47",
                    "LayerId": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc"
                }
            ]
        },
        {
            "id": "a029e0b6-fe48-4df2-9722-584a86bbe0b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "compositeImage": {
                "id": "c3df3296-32a5-48f5-b911-1cde62bdec06",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a029e0b6-fe48-4df2-9722-584a86bbe0b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e98baec-c7ea-4628-9ed0-bbc890efdf38",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a029e0b6-fe48-4df2-9722-584a86bbe0b9",
                    "LayerId": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "499c04ee-2bf2-4eca-ae9e-9f0c2aee93bc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d7ace56-d154-4b81-bfd8-c0f7821b3856",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}