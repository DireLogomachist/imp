{
    "id": "b8e8d6ad-cb83-46db-b5c8-2ad3b135c97e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newspaper_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 211,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f4fd3b8b-00ab-4a14-b418-f01d51a393e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8e8d6ad-cb83-46db-b5c8-2ad3b135c97e",
            "compositeImage": {
                "id": "13a5209f-6bb3-4d74-8203-9c059cd3cc73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f4fd3b8b-00ab-4a14-b418-f01d51a393e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fed4ec3-0e6e-4ae8-bd81-6703b2bb9801",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f4fd3b8b-00ab-4a14-b418-f01d51a393e0",
                    "LayerId": "2c1d7add-05f6-47d7-af0d-d0f6e5f02c92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "2c1d7add-05f6-47d7-af0d-d0f6e5f02c92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8e8d6ad-cb83-46db-b5c8-2ad3b135c97e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 212,
    "xorig": 0,
    "yorig": 0
}