{
    "id": "a041ceed-a5b1-4552-a81b-09ae239cff92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_possession_circle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 31,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71ed12a7-2160-464f-838e-678b763f0b7b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a041ceed-a5b1-4552-a81b-09ae239cff92",
            "compositeImage": {
                "id": "7ee69875-141d-46c0-98a7-4fe30f6bcfcd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71ed12a7-2160-464f-838e-678b763f0b7b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b427540-5391-4416-a328-8e053f2bb6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71ed12a7-2160-464f-838e-678b763f0b7b",
                    "LayerId": "84fc2f91-819c-47d8-bd3c-b30ed2dda169"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "84fc2f91-819c-47d8-bd3c-b30ed2dda169",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a041ceed-a5b1-4552-a81b-09ae239cff92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 32
}