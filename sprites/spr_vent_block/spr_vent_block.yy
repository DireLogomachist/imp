{
    "id": "996efd4a-d60d-47ca-a168-ce817d5e57ac",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vent_block",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fe21c14-fc04-4121-b171-bdd44f12c5c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "996efd4a-d60d-47ca-a168-ce817d5e57ac",
            "compositeImage": {
                "id": "aac2b1d3-366e-4e0e-99e7-c9c07cb41041",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fe21c14-fc04-4121-b171-bdd44f12c5c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19366aeb-bfde-4355-a491-ab930bbf76af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fe21c14-fc04-4121-b171-bdd44f12c5c5",
                    "LayerId": "ed3efe68-d0b2-4edd-b071-9482cbc6a8cd"
                }
            ]
        },
        {
            "id": "41dd8e0b-4a11-4daf-9611-f8b0ac506f19",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "996efd4a-d60d-47ca-a168-ce817d5e57ac",
            "compositeImage": {
                "id": "667cc614-71cc-4214-9e86-febc3cb910da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41dd8e0b-4a11-4daf-9611-f8b0ac506f19",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "525bcad2-22f7-4874-ac27-3b6aaa71d86e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41dd8e0b-4a11-4daf-9611-f8b0ac506f19",
                    "LayerId": "ed3efe68-d0b2-4edd-b071-9482cbc6a8cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ed3efe68-d0b2-4edd-b071-9482cbc6a8cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "996efd4a-d60d-47ca-a168-ce817d5e57ac",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 16,
    "yorig": 8
}