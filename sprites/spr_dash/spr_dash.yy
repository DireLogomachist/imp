{
    "id": "ba937f1c-3cfd-4d9a-8abc-321e0d91cdce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 10,
    "bbox_right": 52,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1020ec9a-fee5-4d2e-b4b9-26dbcfa55e10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba937f1c-3cfd-4d9a-8abc-321e0d91cdce",
            "compositeImage": {
                "id": "b1c3c411-3ded-43bc-969c-f0d7a43f5e78",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1020ec9a-fee5-4d2e-b4b9-26dbcfa55e10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3749e096-481c-448c-9eae-4969c99abb91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1020ec9a-fee5-4d2e-b4b9-26dbcfa55e10",
                    "LayerId": "a6d29424-4f45-452b-80f6-cc2870dfea69"
                }
            ]
        },
        {
            "id": "61187b57-ac17-4992-b952-d9b6332d1a4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba937f1c-3cfd-4d9a-8abc-321e0d91cdce",
            "compositeImage": {
                "id": "dd03d2d8-5ce4-4b91-becc-e8181f20f022",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61187b57-ac17-4992-b952-d9b6332d1a4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce0ef990-321e-4d03-8749-e61e342f000b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61187b57-ac17-4992-b952-d9b6332d1a4e",
                    "LayerId": "a6d29424-4f45-452b-80f6-cc2870dfea69"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a6d29424-4f45-452b-80f6-cc2870dfea69",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba937f1c-3cfd-4d9a-8abc-321e0d91cdce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}