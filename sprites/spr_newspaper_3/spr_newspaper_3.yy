{
    "id": "91277919-fda6-41da-94c2-57a84a02c359",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newspaper_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cfadfda9-ae4d-40eb-825e-e786b7c44742",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "91277919-fda6-41da-94c2-57a84a02c359",
            "compositeImage": {
                "id": "862ca933-b185-412e-a516-e5195517a79e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cfadfda9-ae4d-40eb-825e-e786b7c44742",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8744b1ba-20ae-4ff2-8027-c30bb91c0e55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cfadfda9-ae4d-40eb-825e-e786b7c44742",
                    "LayerId": "be330aaf-247d-490e-8cee-3300b87b5e60"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "be330aaf-247d-490e-8cee-3300b87b5e60",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "91277919-fda6-41da-94c2-57a84a02c359",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 220,
    "xorig": 0,
    "yorig": 0
}