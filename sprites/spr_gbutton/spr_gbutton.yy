{
    "id": "2d354c93-9659-4257-a1a3-9a7445de5fc4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gbutton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 10,
    "bbox_right": 19,
    "bbox_top": 23,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e852ab7a-49d5-4d3d-88c2-a310f10be560",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d354c93-9659-4257-a1a3-9a7445de5fc4",
            "compositeImage": {
                "id": "8da5a975-1a1a-45e1-aa40-6928f21b0dc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e852ab7a-49d5-4d3d-88c2-a310f10be560",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3558b61d-6547-4b5c-b3be-4a114ef1f3b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e852ab7a-49d5-4d3d-88c2-a310f10be560",
                    "LayerId": "c0bd0f2c-09ff-47f7-a609-4e7de0519fcf"
                }
            ]
        },
        {
            "id": "dd300fb1-2e28-4946-835d-07125c52a78f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d354c93-9659-4257-a1a3-9a7445de5fc4",
            "compositeImage": {
                "id": "c9e5d5f9-4e1f-46dc-a50e-8a1c4da2f7da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd300fb1-2e28-4946-835d-07125c52a78f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca81ee0-a8b1-404e-a850-2158a3b97696",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd300fb1-2e28-4946-835d-07125c52a78f",
                    "LayerId": "c0bd0f2c-09ff-47f7-a609-4e7de0519fcf"
                }
            ]
        },
        {
            "id": "77fb7df0-39f5-417a-b6a0-7a5484e9f226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d354c93-9659-4257-a1a3-9a7445de5fc4",
            "compositeImage": {
                "id": "3c35c5f1-ced4-4969-b42a-0881970d1b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "77fb7df0-39f5-417a-b6a0-7a5484e9f226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2363c4b-2868-4730-a720-c4280d73ae66",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "77fb7df0-39f5-417a-b6a0-7a5484e9f226",
                    "LayerId": "c0bd0f2c-09ff-47f7-a609-4e7de0519fcf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c0bd0f2c-09ff-47f7-a609-4e7de0519fcf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d354c93-9659-4257-a1a3-9a7445de5fc4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 16
}