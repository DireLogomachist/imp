{
    "id": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exec",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 18,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e9923cca-6178-4541-9cf8-8a6df6720a75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
            "compositeImage": {
                "id": "1886e534-fe50-4b06-93cd-e5d16a88a099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9923cca-6178-4541-9cf8-8a6df6720a75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "340bdc35-d165-4df0-8ba1-733bd7210be5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9923cca-6178-4541-9cf8-8a6df6720a75",
                    "LayerId": "f36deae0-a418-4c3e-879d-d1d89b98bf0c"
                }
            ]
        },
        {
            "id": "e44cfd1d-0e70-4432-a755-19394b137895",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
            "compositeImage": {
                "id": "a7897409-e139-409d-a610-f50a971dbba6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e44cfd1d-0e70-4432-a755-19394b137895",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01ca6759-a60e-4bde-8d3f-ad1b7d0a951e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e44cfd1d-0e70-4432-a755-19394b137895",
                    "LayerId": "f36deae0-a418-4c3e-879d-d1d89b98bf0c"
                }
            ]
        },
        {
            "id": "43c6a34f-8afb-46f8-a4fa-0a6a3a51c748",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
            "compositeImage": {
                "id": "d58683e1-65b6-4a1d-949a-41d4bb6a0a42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43c6a34f-8afb-46f8-a4fa-0a6a3a51c748",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "344dbc9a-a32d-41e4-8b68-9e558867ece1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43c6a34f-8afb-46f8-a4fa-0a6a3a51c748",
                    "LayerId": "f36deae0-a418-4c3e-879d-d1d89b98bf0c"
                }
            ]
        },
        {
            "id": "edae649a-46ad-4b32-83ff-ba5afaf5009d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
            "compositeImage": {
                "id": "fb4ade75-ef25-4966-9173-85f3323c6917",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edae649a-46ad-4b32-83ff-ba5afaf5009d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a3b415a-d0f1-46bf-8e82-58a4e029aafc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edae649a-46ad-4b32-83ff-ba5afaf5009d",
                    "LayerId": "f36deae0-a418-4c3e-879d-d1d89b98bf0c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f36deae0-a418-4c3e-879d-d1d89b98bf0c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6becd88c-50d2-40df-ad4e-c2c9796e4ed5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 20
}