{
    "id": "a4f21ffd-628d-4a11-a0f8-16969f409e64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newspaper_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 199,
    "bbox_left": 0,
    "bbox_right": 167,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "55164535-2b45-4104-8574-3c32a4629902",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4f21ffd-628d-4a11-a0f8-16969f409e64",
            "compositeImage": {
                "id": "8ce6eadd-4f8a-4d17-9917-c6c07a68f23c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55164535-2b45-4104-8574-3c32a4629902",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8df75387-288b-43e9-907c-154c7bd1aedb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55164535-2b45-4104-8574-3c32a4629902",
                    "LayerId": "f1b3ad05-ead3-4b50-946a-ca5bd5ecf8a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 200,
    "layers": [
        {
            "id": "f1b3ad05-ead3-4b50-946a-ca5bd5ecf8a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4f21ffd-628d-4a11-a0f8-16969f409e64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 0,
    "yorig": 0
}