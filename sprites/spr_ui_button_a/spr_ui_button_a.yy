{
    "id": "05ee7a46-dc3f-490f-8562-bbff85c864b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_button_a",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d7e37ef1-6cbc-42e8-80f7-318b57ef5b27",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "05ee7a46-dc3f-490f-8562-bbff85c864b4",
            "compositeImage": {
                "id": "18d0928f-731b-447e-9da1-7c0ebb911cde",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7e37ef1-6cbc-42e8-80f7-318b57ef5b27",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90160034-d4a4-4b26-a9a3-ffa2d5060417",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7e37ef1-6cbc-42e8-80f7-318b57ef5b27",
                    "LayerId": "05a2b315-a431-4880-9634-155124ee4531"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "05a2b315-a431-4880-9634-155124ee4531",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "05ee7a46-dc3f-490f-8562-bbff85c864b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}