{
    "id": "45f9f3eb-2da3-403c-a46b-7e6260ef42fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_smoke_particle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 5,
    "bbox_right": 11,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b3fb0fb1-e76f-4af2-8b9a-383ad9eddcdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "45f9f3eb-2da3-403c-a46b-7e6260ef42fc",
            "compositeImage": {
                "id": "c229be4a-73c1-48e0-a494-3d48f5620188",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b3fb0fb1-e76f-4af2-8b9a-383ad9eddcdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b9d3bbe-b658-4a0c-9394-876bb9fa084c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b3fb0fb1-e76f-4af2-8b9a-383ad9eddcdc",
                    "LayerId": "54418b35-43e9-45f8-a189-364575fe4301"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "54418b35-43e9-45f8-a189-364575fe4301",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "45f9f3eb-2da3-403c-a46b-7e6260ef42fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}