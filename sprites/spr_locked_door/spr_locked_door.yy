{
    "id": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_locked_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 47,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75db3a57-2b79-4760-8702-6e17c124ede3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "compositeImage": {
                "id": "00677a7c-12ba-45c1-b203-de5d08e71122",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75db3a57-2b79-4760-8702-6e17c124ede3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ccbceaa2-d185-4c96-a1ad-ae9223e5a6d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75db3a57-2b79-4760-8702-6e17c124ede3",
                    "LayerId": "799730b7-87d8-4f70-994b-3361c035aeec"
                }
            ]
        },
        {
            "id": "bf263e18-b5b7-4075-be1d-46e3f1d9fd0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "compositeImage": {
                "id": "840673c9-34be-411f-80c3-f0f0a5436fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf263e18-b5b7-4075-be1d-46e3f1d9fd0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6059149c-133c-4db4-9d99-ff4c3092f410",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf263e18-b5b7-4075-be1d-46e3f1d9fd0f",
                    "LayerId": "799730b7-87d8-4f70-994b-3361c035aeec"
                }
            ]
        },
        {
            "id": "e4bb147d-5e8a-4e9c-b54d-e3004c7a1010",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "compositeImage": {
                "id": "8542b7b1-e818-44e0-aa06-596d275ec2b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e4bb147d-5e8a-4e9c-b54d-e3004c7a1010",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2015bc47-b76e-4746-b077-397bbeb3f242",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e4bb147d-5e8a-4e9c-b54d-e3004c7a1010",
                    "LayerId": "799730b7-87d8-4f70-994b-3361c035aeec"
                }
            ]
        },
        {
            "id": "3d3dc5f6-fe80-46bd-b78d-b0bb62cbfcb5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "compositeImage": {
                "id": "969e67b5-d7fb-4dec-8622-62aac36d4d5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d3dc5f6-fe80-46bd-b78d-b0bb62cbfcb5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3250bbc7-528e-4f53-afa6-5214ab7d9831",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d3dc5f6-fe80-46bd-b78d-b0bb62cbfcb5",
                    "LayerId": "799730b7-87d8-4f70-994b-3361c035aeec"
                }
            ]
        },
        {
            "id": "e487a3d8-44dc-4ac2-8e56-a220694a892c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "compositeImage": {
                "id": "7a98d956-69e2-45ac-9041-199f4bb67217",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e487a3d8-44dc-4ac2-8e56-a220694a892c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eefb0992-7d5c-41f3-9d79-3d7f4a96fc0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e487a3d8-44dc-4ac2-8e56-a220694a892c",
                    "LayerId": "799730b7-87d8-4f70-994b-3361c035aeec"
                }
            ]
        },
        {
            "id": "b09ccb82-ef04-47c1-a411-a370f2806518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "compositeImage": {
                "id": "2eb2b348-b83b-4dac-873b-230b1d21e348",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b09ccb82-ef04-47c1-a411-a370f2806518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71f4c54a-30b6-49a0-a3c5-e41de012ef05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b09ccb82-ef04-47c1-a411-a370f2806518",
                    "LayerId": "799730b7-87d8-4f70-994b-3361c035aeec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "799730b7-87d8-4f70-994b-3361c035aeec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "803f5419-ebf5-461c-a7f8-c7fbfbcf6643",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}