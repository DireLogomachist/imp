{
    "id": "3de70d24-f037-4443-9def-1fdb9d452b28",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blood_flecks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 12,
    "bbox_right": 23,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a33b853-1403-4cdc-8472-79db24d24267",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
            "compositeImage": {
                "id": "0cb88b09-d7d2-426a-8797-be496a9cfde8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a33b853-1403-4cdc-8472-79db24d24267",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aa4aa0db-4e3c-4034-b6de-09a15c9d0e4a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a33b853-1403-4cdc-8472-79db24d24267",
                    "LayerId": "f2f48fa8-dd10-4200-88e3-92851f295cb0"
                }
            ]
        },
        {
            "id": "5b0df4f9-f401-4838-ab7c-11f4056f038f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
            "compositeImage": {
                "id": "e3137f3b-c91b-45cb-bf47-a35459286d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b0df4f9-f401-4838-ab7c-11f4056f038f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e93e140-8e37-4b63-ae43-f3051519b931",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b0df4f9-f401-4838-ab7c-11f4056f038f",
                    "LayerId": "f2f48fa8-dd10-4200-88e3-92851f295cb0"
                }
            ]
        },
        {
            "id": "bf202862-8fb5-4332-8149-012f1c7a17e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
            "compositeImage": {
                "id": "c208193d-c138-4447-8da4-bd58002dcf22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf202862-8fb5-4332-8149-012f1c7a17e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cce00db-a42d-488e-a51d-cc4267829360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf202862-8fb5-4332-8149-012f1c7a17e7",
                    "LayerId": "f2f48fa8-dd10-4200-88e3-92851f295cb0"
                }
            ]
        },
        {
            "id": "80462509-2889-41e8-a4ae-b1882152c012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
            "compositeImage": {
                "id": "d9f98f42-7148-4eec-8e59-bd5d599aeca7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "80462509-2889-41e8-a4ae-b1882152c012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2f51a42-46f5-47b2-a8a8-0f2c6f34aae6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "80462509-2889-41e8-a4ae-b1882152c012",
                    "LayerId": "f2f48fa8-dd10-4200-88e3-92851f295cb0"
                }
            ]
        },
        {
            "id": "7fb91365-d09d-4d5b-95d2-74e3653f4d2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
            "compositeImage": {
                "id": "b7154a4c-7c20-4a7a-b391-832e3a073e65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fb91365-d09d-4d5b-95d2-74e3653f4d2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "115ed96b-52a4-4fbb-9bb7-b0e186ab4dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fb91365-d09d-4d5b-95d2-74e3653f4d2f",
                    "LayerId": "f2f48fa8-dd10-4200-88e3-92851f295cb0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f2f48fa8-dd10-4200-88e3-92851f295cb0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3de70d24-f037-4443-9def-1fdb9d452b28",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}