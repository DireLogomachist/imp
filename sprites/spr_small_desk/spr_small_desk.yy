{
    "id": "23127a0e-0207-495f-adfd-bed0dbce3589",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_small_desk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1213120f-851e-4eef-b125-df245c1d893a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23127a0e-0207-495f-adfd-bed0dbce3589",
            "compositeImage": {
                "id": "7e09fdee-0f5a-46b2-96b1-b5b8b82c1394",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1213120f-851e-4eef-b125-df245c1d893a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4068d73-fa0c-4a44-a6b4-1044b120af25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1213120f-851e-4eef-b125-df245c1d893a",
                    "LayerId": "8d1c1dde-e6d0-4636-af6a-b9cdd771be07"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8d1c1dde-e6d0-4636-af6a-b9cdd771be07",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23127a0e-0207-495f-adfd-bed0dbce3589",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 16
}