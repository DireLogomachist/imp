{
    "id": "9687deba-28d6-4f14-b691-1c22f6fe6788",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_window",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c6175d6-8eab-49e1-a7dd-bf076f66aa1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9687deba-28d6-4f14-b691-1c22f6fe6788",
            "compositeImage": {
                "id": "6a931044-8851-47b8-9cbf-4d0054a63a7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c6175d6-8eab-49e1-a7dd-bf076f66aa1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10453602-2ba9-4e84-8abc-a5c81d3b7a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c6175d6-8eab-49e1-a7dd-bf076f66aa1d",
                    "LayerId": "1f5f8809-05f4-43c8-9416-ee0a2b2c8f95"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1f5f8809-05f4-43c8-9416-ee0a2b2c8f95",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9687deba-28d6-4f14-b691-1c22f6fe6788",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}