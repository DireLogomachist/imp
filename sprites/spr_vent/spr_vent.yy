{
    "id": "46ed9c78-04da-4218-aeee-e314e8426e9e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 5,
    "bbox_right": 27,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "370e2fb6-4023-4c3b-9b57-078c42e26921",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46ed9c78-04da-4218-aeee-e314e8426e9e",
            "compositeImage": {
                "id": "19acbdbf-a903-49bc-8672-8c9c7b467a30",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "370e2fb6-4023-4c3b-9b57-078c42e26921",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be9d5d52-30a2-433b-9828-69d3ff0f8a60",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "370e2fb6-4023-4c3b-9b57-078c42e26921",
                    "LayerId": "1c4fcb5e-2cc5-4013-840d-5d94671ba06e"
                }
            ]
        },
        {
            "id": "08441072-c6b3-453a-8d3e-d538b8b4c001",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "46ed9c78-04da-4218-aeee-e314e8426e9e",
            "compositeImage": {
                "id": "0c02327e-96ce-42ad-aad5-56d32d921085",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08441072-c6b3-453a-8d3e-d538b8b4c001",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fc46604-de76-465b-9d8d-3d0505a36354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08441072-c6b3-453a-8d3e-d538b8b4c001",
                    "LayerId": "1c4fcb5e-2cc5-4013-840d-5d94671ba06e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1c4fcb5e-2cc5-4013-840d-5d94671ba06e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "46ed9c78-04da-4218-aeee-e314e8426e9e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}