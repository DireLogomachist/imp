{
    "id": "0fbbb2dd-23e9-42c7-856d-0350a6e51e81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_laser_u",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 4,
    "bbox_right": 10,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "998fea8a-593c-4b23-aada-f0593d30a20d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0fbbb2dd-23e9-42c7-856d-0350a6e51e81",
            "compositeImage": {
                "id": "0867b78c-a038-4c43-9cff-5175899450ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "998fea8a-593c-4b23-aada-f0593d30a20d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4941b778-ec1c-4bf7-b4c1-1fed6dafca18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "998fea8a-593c-4b23-aada-f0593d30a20d",
                    "LayerId": "398f6814-2c04-40be-8d94-cd3803b88a08"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "398f6814-2c04-40be-8d94-cd3803b88a08",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0fbbb2dd-23e9-42c7-856d-0350a6e51e81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 15
}