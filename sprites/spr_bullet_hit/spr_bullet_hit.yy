{
    "id": "5c3da86f-ced9-4191-a410-379ce0544daa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet_hit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 13,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6caf40b4-0a48-4ba0-a907-7fce82c0065e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c3da86f-ced9-4191-a410-379ce0544daa",
            "compositeImage": {
                "id": "bd726a5f-f7e7-4d71-8658-be7baac22eef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6caf40b4-0a48-4ba0-a907-7fce82c0065e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c3ac6c2-124f-449f-8342-6975ea460097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6caf40b4-0a48-4ba0-a907-7fce82c0065e",
                    "LayerId": "d5bff29b-5f97-49a0-bc64-52298ab77e8f"
                }
            ]
        },
        {
            "id": "c9f46cb2-5f62-4340-a3dc-88ccb5cb8b05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c3da86f-ced9-4191-a410-379ce0544daa",
            "compositeImage": {
                "id": "8b970683-e787-433d-aff2-346d23998be5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c9f46cb2-5f62-4340-a3dc-88ccb5cb8b05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb327975-aa31-4d95-a11d-b08c19d3217f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c9f46cb2-5f62-4340-a3dc-88ccb5cb8b05",
                    "LayerId": "d5bff29b-5f97-49a0-bc64-52298ab77e8f"
                }
            ]
        },
        {
            "id": "b8d4be51-49a7-40cd-947f-94d7a728c425",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c3da86f-ced9-4191-a410-379ce0544daa",
            "compositeImage": {
                "id": "eac3fa03-2c31-429b-8321-22e7ea7e6a58",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b8d4be51-49a7-40cd-947f-94d7a728c425",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7bf59f6-2b8e-4443-ba82-e7ed940f671a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b8d4be51-49a7-40cd-947f-94d7a728c425",
                    "LayerId": "d5bff29b-5f97-49a0-bc64-52298ab77e8f"
                }
            ]
        },
        {
            "id": "1613d95b-92d4-4851-8834-0f606135bd12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c3da86f-ced9-4191-a410-379ce0544daa",
            "compositeImage": {
                "id": "f18de446-34dd-4675-9ed8-b13511530c85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1613d95b-92d4-4851-8834-0f606135bd12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6e714ea-b958-4e85-84e6-194812577caa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1613d95b-92d4-4851-8834-0f606135bd12",
                    "LayerId": "d5bff29b-5f97-49a0-bc64-52298ab77e8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "d5bff29b-5f97-49a0-bc64-52298ab77e8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c3da86f-ced9-4191-a410-379ce0544daa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 12
}