{
    "id": "eee19682-44a7-4d9e-9d59-44bd68a00098",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 38,
    "bbox_right": 209,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0bfea0a-de23-4551-8c07-d937bf5ecf82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eee19682-44a7-4d9e-9d59-44bd68a00098",
            "compositeImage": {
                "id": "f1fdeab4-580a-44e0-b83c-46a3347e1cf8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bfea0a-de23-4551-8c07-d937bf5ecf82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31e12795-ea77-4afd-a21d-bd115902b504",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bfea0a-de23-4551-8c07-d937bf5ecf82",
                    "LayerId": "dde08eb8-672d-423b-baf8-d9d2af15ad92"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 158,
    "layers": [
        {
            "id": "dde08eb8-672d-423b-baf8-d9d2af15ad92",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eee19682-44a7-4d9e-9d59-44bd68a00098",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 238,
    "xorig": 0,
    "yorig": 0
}