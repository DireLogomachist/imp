{
    "id": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guard_gun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 10,
    "bbox_left": 5,
    "bbox_right": 10,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "78aa43e0-5886-43ee-8d3b-de97a6c685b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "2e9a77df-0757-4270-95d5-fde90d7734a6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78aa43e0-5886-43ee-8d3b-de97a6c685b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "47660cb2-0ab7-4920-b129-1ffe35390ca9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78aa43e0-5886-43ee-8d3b-de97a6c685b6",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "acbda610-3f76-4054-ab4a-88caeb4e7627",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "00a923d7-0ee2-4ce9-a784-67de9599fea4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "acbda610-3f76-4054-ab4a-88caeb4e7627",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "580ff572-4c38-4993-a754-a654e1dab2c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "acbda610-3f76-4054-ab4a-88caeb4e7627",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "7f687958-5702-42f5-aac4-f411d317211a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "bcc5c860-3c45-48c7-a202-611ee6698c7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f687958-5702-42f5-aac4-f411d317211a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "864cc7cc-ee2d-408e-a25c-5a49377880e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f687958-5702-42f5-aac4-f411d317211a",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "7478453a-2366-486c-8cdd-09be04f05cbd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "65341e72-2610-4b98-a935-9acf958f95ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7478453a-2366-486c-8cdd-09be04f05cbd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68c3bd0b-5e29-4035-b16b-2c7937188afa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7478453a-2366-486c-8cdd-09be04f05cbd",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "0a53ef5b-069a-4bb4-8bbd-176ce09300a6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "dac5bce6-777c-4ed9-90fa-d2d38a928d2c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a53ef5b-069a-4bb4-8bbd-176ce09300a6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "931fba65-645c-45c6-b4d4-33de3e7075b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a53ef5b-069a-4bb4-8bbd-176ce09300a6",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "0e88ab6d-d5c8-4e88-9076-d4907f4fa505",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "fc542b53-3811-488a-a01d-80f6336dcfa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e88ab6d-d5c8-4e88-9076-d4907f4fa505",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c78478bf-6717-482c-b208-29c9f0f1a2b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e88ab6d-d5c8-4e88-9076-d4907f4fa505",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "6c529350-ab1e-4ca7-b599-3485bae8fc03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "872b792f-6eb1-4f20-a65b-bb9286598678",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c529350-ab1e-4ca7-b599-3485bae8fc03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c4d2dc-246c-4af9-b0f4-a1d22239b564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c529350-ab1e-4ca7-b599-3485bae8fc03",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        },
        {
            "id": "ed1004df-9571-4734-94cc-2dc84268bfbc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "compositeImage": {
                "id": "1a9417a2-409c-4b2b-b30c-87083a12d6f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed1004df-9571-4734-94cc-2dc84268bfbc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6d9730f1-c471-40e3-8fad-299040eb82f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed1004df-9571-4734-94cc-2dc84268bfbc",
                    "LayerId": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "38c5a2c3-02de-4e48-aa0d-836e353b0ca6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22a0f2a9-9de8-4512-bc68-ab5ab901850e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}