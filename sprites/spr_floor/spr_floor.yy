{
    "id": "9626ee61-2489-4d8f-a39d-cbd0b528c362",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_floor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f28ffa73-01b7-425d-90ea-e683498babd1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9626ee61-2489-4d8f-a39d-cbd0b528c362",
            "compositeImage": {
                "id": "4d25099e-216e-4731-a023-6671e08b372b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f28ffa73-01b7-425d-90ea-e683498babd1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d8ba8e96-7896-4310-a438-cc047d5ae8a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f28ffa73-01b7-425d-90ea-e683498babd1",
                    "LayerId": "bf2cb8f8-95b6-4f15-bd6d-b13a6c41e464"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "bf2cb8f8-95b6-4f15-bd6d-b13a6c41e464",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9626ee61-2489-4d8f-a39d-cbd0b528c362",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}