{
    "id": "3ad1c185-4a01-4ae2-8119-41bc42d1359a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newspaper_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 211,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a55ff5f-f18e-4316-9e24-978d3a22ed32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ad1c185-4a01-4ae2-8119-41bc42d1359a",
            "compositeImage": {
                "id": "4e117c0f-4955-4f0a-b61e-7498bf9c5076",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a55ff5f-f18e-4316-9e24-978d3a22ed32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6cf9edcc-1488-46c5-b8f4-631fe48d3395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a55ff5f-f18e-4316-9e24-978d3a22ed32",
                    "LayerId": "617c6264-1252-47cd-881b-33b5e6a96e36"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "617c6264-1252-47cd-881b-33b5e6a96e36",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ad1c185-4a01-4ae2-8119-41bc42d1359a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 212,
    "xorig": 0,
    "yorig": 0
}