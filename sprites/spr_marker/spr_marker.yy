{
    "id": "9782a88f-0a33-4dc4-bddb-02a15ed84a00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 16,
    "bbox_left": 14,
    "bbox_right": 16,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b38637e-53dc-4834-9713-b1cf3eb314c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9782a88f-0a33-4dc4-bddb-02a15ed84a00",
            "compositeImage": {
                "id": "4b6b56bf-d5b5-49d2-a588-1bb3f0683e52",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b38637e-53dc-4834-9713-b1cf3eb314c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e739ad48-0549-40d1-b826-1bf7ac3eb4f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b38637e-53dc-4834-9713-b1cf3eb314c4",
                    "LayerId": "75dc99c9-fe0a-4dbe-b39a-d382a249e36c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "75dc99c9-fe0a-4dbe-b39a-d382a249e36c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9782a88f-0a33-4dc4-bddb-02a15ed84a00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 15
}