{
    "id": "656c18dd-e362-4192-9207-b1ab123afb7b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_vent_path",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fb6409d2-985a-4c0f-88ca-3e08c57af6a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
            "compositeImage": {
                "id": "9ebfa532-9b8c-4426-8fd0-425a1096690d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fb6409d2-985a-4c0f-88ca-3e08c57af6a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "657d713f-ad6c-4c4a-8155-432b6536bd5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fb6409d2-985a-4c0f-88ca-3e08c57af6a5",
                    "LayerId": "e5abacfc-cc9b-4ef8-ab9c-15f8686f933b"
                }
            ]
        },
        {
            "id": "06ef5cb5-7d8a-4d6b-b6e4-e07cd4d3a483",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
            "compositeImage": {
                "id": "5ca10904-ea78-4c20-9fd2-6fb13dce5a50",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "06ef5cb5-7d8a-4d6b-b6e4-e07cd4d3a483",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b20082c-657e-444c-ae05-85efd496d456",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "06ef5cb5-7d8a-4d6b-b6e4-e07cd4d3a483",
                    "LayerId": "e5abacfc-cc9b-4ef8-ab9c-15f8686f933b"
                }
            ]
        },
        {
            "id": "7b79d63c-e237-4527-b4a0-90fc4b160247",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
            "compositeImage": {
                "id": "e0ffbde6-4f57-4190-9868-e77b8be2a15f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b79d63c-e237-4527-b4a0-90fc4b160247",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a42b5598-d191-4cfa-8e5f-2a76a9697907",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b79d63c-e237-4527-b4a0-90fc4b160247",
                    "LayerId": "e5abacfc-cc9b-4ef8-ab9c-15f8686f933b"
                }
            ]
        },
        {
            "id": "09ff536b-ad8d-44c3-bc07-c32e014772dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
            "compositeImage": {
                "id": "20075227-8504-4e5e-8e65-1f4443128e8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09ff536b-ad8d-44c3-bc07-c32e014772dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b9efaf1-2b49-484e-8246-71db1e32a1f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09ff536b-ad8d-44c3-bc07-c32e014772dd",
                    "LayerId": "e5abacfc-cc9b-4ef8-ab9c-15f8686f933b"
                }
            ]
        },
        {
            "id": "86da704d-e1dc-4d7f-9f32-017b03d4fe4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
            "compositeImage": {
                "id": "0349ee16-32a9-4e3f-9f6d-300b8cdb239c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86da704d-e1dc-4d7f-9f32-017b03d4fe4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "782941cd-028b-4e2e-972b-64883b5218c9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86da704d-e1dc-4d7f-9f32-017b03d4fe4b",
                    "LayerId": "e5abacfc-cc9b-4ef8-ab9c-15f8686f933b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e5abacfc-cc9b-4ef8-ab9c-15f8686f933b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "656c18dd-e362-4192-9207-b1ab123afb7b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}