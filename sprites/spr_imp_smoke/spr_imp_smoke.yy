{
    "id": "270854cc-e5e9-46ba-9338-881acad805fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_imp_smoke",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 6,
    "bbox_right": 24,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3ee6126-6c51-493c-a031-65c5da5ee57f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "270854cc-e5e9-46ba-9338-881acad805fc",
            "compositeImage": {
                "id": "103864bc-ac62-420e-972e-c88460c48119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3ee6126-6c51-493c-a031-65c5da5ee57f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4697e065-fa74-40c4-83d6-90f6afd15ad9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3ee6126-6c51-493c-a031-65c5da5ee57f",
                    "LayerId": "9c52a000-1ad4-457c-8b7e-81814bf0a249"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9c52a000-1ad4-457c-8b7e-81814bf0a249",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "270854cc-e5e9-46ba-9338-881acad805fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}