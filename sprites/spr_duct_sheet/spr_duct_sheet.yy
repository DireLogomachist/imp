{
    "id": "b4cbe073-38de-494c-8b6f-544efd05f412",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_duct_sheet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0025fe4-e227-42c7-a7bf-49411e9e4124",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b4cbe073-38de-494c-8b6f-544efd05f412",
            "compositeImage": {
                "id": "254dff56-aae5-4506-9ae0-961dd8a7e74c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0025fe4-e227-42c7-a7bf-49411e9e4124",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "966d596e-166d-40fd-b44b-3df28f883160",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0025fe4-e227-42c7-a7bf-49411e9e4124",
                    "LayerId": "d19e3afb-b115-4a5b-abd7-c3a2fdd8a112"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "d19e3afb-b115-4a5b-abd7-c3a2fdd8a112",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b4cbe073-38de-494c-8b6f-544efd05f412",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}