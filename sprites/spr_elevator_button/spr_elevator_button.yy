{
    "id": "8fa67c6c-ded5-4b81-888c-72bddf3adeba",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_elevator_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 23,
    "bbox_right": 29,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36223c6b-9f68-4644-867f-64a2faada191",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa67c6c-ded5-4b81-888c-72bddf3adeba",
            "compositeImage": {
                "id": "db2bd7ba-b172-4ed2-a3be-d75cf208eeea",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36223c6b-9f68-4644-867f-64a2faada191",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54548914-fa4f-4fb0-88f6-162764b78f06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36223c6b-9f68-4644-867f-64a2faada191",
                    "LayerId": "0ad2860c-315f-4caa-8db1-9eaebba04f4a"
                }
            ]
        },
        {
            "id": "91c45382-d498-4ce8-8450-b1d285ceb4c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8fa67c6c-ded5-4b81-888c-72bddf3adeba",
            "compositeImage": {
                "id": "27db30c6-6456-4ba8-82b9-5a274fd14844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91c45382-d498-4ce8-8450-b1d285ceb4c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d8c63d7-849c-4dbd-96cc-602bdbd40579",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91c45382-d498-4ce8-8450-b1d285ceb4c4",
                    "LayerId": "0ad2860c-315f-4caa-8db1-9eaebba04f4a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0ad2860c-315f-4caa-8db1-9eaebba04f4a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8fa67c6c-ded5-4b81-888c-72bddf3adeba",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}