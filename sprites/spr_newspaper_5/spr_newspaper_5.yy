{
    "id": "34254d6f-c8d5-4050-845f-fb7e0bb64506",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_newspaper_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 155,
    "bbox_left": 0,
    "bbox_right": 219,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00dbd57b-3b07-42e1-b981-576a1960b318",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "34254d6f-c8d5-4050-845f-fb7e0bb64506",
            "compositeImage": {
                "id": "6c6f505c-d330-4795-916d-7992ff1b6706",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00dbd57b-3b07-42e1-b981-576a1960b318",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cbe2f32a-847d-41c0-994d-d58f0401658d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00dbd57b-3b07-42e1-b981-576a1960b318",
                    "LayerId": "d2a43095-e08f-4c02-94b9-a83bf7291d82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 156,
    "layers": [
        {
            "id": "d2a43095-e08f-4c02-94b9-a83bf7291d82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "34254d6f-c8d5-4050-845f-fb7e0bb64506",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 220,
    "xorig": 0,
    "yorig": 0
}