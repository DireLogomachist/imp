{
    "id": "cad37f6f-d083-4233-89fe-f93ff1e6c720",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_imp_core",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "630091b5-86d6-4a84-ae06-c53c1070b63a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cad37f6f-d083-4233-89fe-f93ff1e6c720",
            "compositeImage": {
                "id": "e0668399-26a2-4b6f-b90b-9c4971a05512",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "630091b5-86d6-4a84-ae06-c53c1070b63a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5b7ed91b-8070-4c0e-b5eb-62ee8fba658b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "630091b5-86d6-4a84-ae06-c53c1070b63a",
                    "LayerId": "9fcb8a2c-bda4-435c-af92-c35811cdbcef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9fcb8a2c-bda4-435c-af92-c35811cdbcef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cad37f6f-d083-4233-89fe-f93ff1e6c720",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}