{
    "id": "5afcd995-b1fb-4243-99e4-419117955144",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sign",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 39,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9beee84c-c669-4790-bbd7-45cff5665674",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5afcd995-b1fb-4243-99e4-419117955144",
            "compositeImage": {
                "id": "9a0074ce-1e0d-47b7-9bd7-81c86f7af043",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9beee84c-c669-4790-bbd7-45cff5665674",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "926a3c4a-b043-410e-9b9f-7d861af1f981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9beee84c-c669-4790-bbd7-45cff5665674",
                    "LayerId": "796a705f-000f-4c1a-aa6d-2eeb3cd5fb5a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "796a705f-000f-4c1a-aa6d-2eeb3cd5fb5a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5afcd995-b1fb-4243-99e4-419117955144",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}