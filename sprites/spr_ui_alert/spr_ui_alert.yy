{
    "id": "cd466780-f90c-48e8-b574-6ada3bc268f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_alert",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 0,
    "bbox_right": 14,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a2bb65ba-d6a8-492b-bbff-f3e5ff24fa5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cd466780-f90c-48e8-b574-6ada3bc268f1",
            "compositeImage": {
                "id": "aac7e371-0c3b-4a99-94dd-e23ac54777f5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a2bb65ba-d6a8-492b-bbff-f3e5ff24fa5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39e929b1-6116-481c-bdc7-f3980fcb268c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a2bb65ba-d6a8-492b-bbff-f3e5ff24fa5b",
                    "LayerId": "662e866f-c190-4a70-aefe-f2cf5e9d8a7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "662e866f-c190-4a70-aefe-f2cf5e9d8a7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cd466780-f90c-48e8-b574-6ada3bc268f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}