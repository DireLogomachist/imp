///@func check_in_bounds(px, py)
///@description

var px = argument0;
var py = argument1;

var tmp_path = path_add();
if mp_grid_path(global.path_grid, tmp_path, x, y, px, py, 1) {
	return true;
} else {
	return false;
}

path_delete(tmp_path);
