///@description


var door = inst_DDAC133;
with door {
	alarm[0] = 1*room_speed;
}
audio_play_sound(au_door_open, 0, false);

base_gbutton_action();
