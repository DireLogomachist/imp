///@func player_dash()
///@description

dist_traveled = point_distance(dash_start_x, dash_start_y, x, y);
if (dist_traveled < dash_distance) {
	
	var move_speed_this_frame = dash_speed*delta_time/1000000;
	var move_x = lengthdir_x(1, facing_direction);
	var move_y = lengthdir_y(1, facing_direction);

	var move_vec = check_collision(move_x, move_y, move_speed_this_frame);
	var move_final_x = x+lengthdir_x(move_speed_this_frame, point_direction(0,0,move_vec[0],move_vec[1]));
	var move_final_y = y+lengthdir_y(move_speed_this_frame, point_direction(0,0,move_vec[0],move_vec[1]))
	 
	if (move_vec[0] != 0 or move_vec[1] != 0) {
		move(move_speed_this_frame,  point_direction(0,0,move_vec[0],move_vec[1]));
	} else {
		state_switch("Default");
	}

	if place_meeting(move_final_x, move_final_y, static_object) {
		back_out_collision(move_final_x, move_final_y, move_vec);
		state_switch("Default");
	}

	with dash_hitbox {
		var collided = instance_place(x, y, enemy);
		if collided != noone {
			with other {
				possess(collided);
				state_switch("Possessing");
				alarm[1] = action_cooldown;
			}
		}
	}
} else {
	state_switch("Default");
}
