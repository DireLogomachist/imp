///@func player_dash_entry()
///@description


sprite_index = spr_imp_core;
part_emitter_stream(smoke_sys, smoke_em, smoke_part, 15);

if irandom(1) == 0 {
	audio_play_sound(au_dash, 0, false);
} else {
	audio_play_sound(au_dash_2, 0, false);
}

dash_start_x = x;
dash_start_y = y;

obj_shadow.visible = false;

