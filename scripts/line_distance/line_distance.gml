///@func line_distance(px, py, x1, y1, x2, y2)
///@description calculates distance from a point to a finite line segment
///https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment


var px = argument0;
var py = argument1;
var x1 = argument2;
var y1 = argument3;
var x2 = argument4;
var y2 = argument5;


var vx = x2-x1;
var vy = y2-y1;

var u =  ((px-x1)*vx + (py-y1)*vy)/(vx*vx + vy*vy);

if (u > 1) {
	u = 1;
} else if (u < 0) {
	u = 0;
}

var dx = (x1 + u*vx) - px;
var dy = (y1 + u*vy) - py;

return sqrt(dx*dx + dy*dy);
