///@func player_death()
///@description player death


// Hide player sprite, set to non-solid, set death state, and uncheck solid
// Generate splatter sprites
// Set reload level timer on level manager
// Reload level
// Destroy player particle system

if player_alive {
	player_alive = false;
	player_controlled = false;
	visible = false;
	rm = global.rm_manager;
	rm.alarm[0] = rm.player_death_wait;
	
	if possessed_object != noone {
		possessed_object.player_controlled = false;
		possessed_object.visible = false;
		instance_destroy(possessed_object);
		possessed_object = noone;
	}

	var splat = instance_create_layer(x, y, "Splatter", splatter);
	splat.image_xscale = sign(random_range(-1,1));
	
	var fleck1 = instance_create_layer(x, y, "Splatter", blood_fleck);
	var fleck2 = instance_create_layer(x, y, "Splatter", blood_fleck);
	var fleck3 = instance_create_layer(x, y, "Splatter", blood_fleck);
	var fleck4 = instance_create_layer(x, y, "Splatter", blood_fleck);
	fleck2.dir += 90;
	fleck3.dir += 180;
	fleck4.dir += 270;
	
	part_system_destroy(smoke_sys);
	obj_shadow.visible = false;
}
