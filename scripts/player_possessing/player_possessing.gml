///@func player_possessing
///@description

x = possessed_object.x;
y = possessed_object.y;

if keyboard_check_pressed(global.dash_input) and alarm[1] < 0 and possessed_object.object_index != exec {
	explode()
	state_switch("Default");
	alarm[1] = action_cooldown;
	obj_shadow.visible = true;
}
