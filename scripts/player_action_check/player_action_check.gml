//func player_action_check()
//description check for nearby actionable objects


if (action_object and action_object.outlined)
	action_object.image_index = action_object.image_index - 1;
action_object = noone;

for(var i = 0; i < array_length_1d(global.actionable_instances); i++) {
	var inst = global.actionable_instances[i];
	if point_distance(x, y, inst.x + inst.offset_x, inst.y + inst.offset_y) <= self.action_radius {
		if (inst.action != "") {
			action_object = inst;
			if (action_object.outlined) action_object.image_index = 1;
		}
	}
}

if keyboard_check_pressed(global.action_input) and action_object != noone {
	with action_object {
		if (outlined) image_index = image_index-1;
		script_execute(self.action, self);
	}
	action_object = noone;
}

// Update controls gui
//if action_object != noone {
	// Set opaque
//} else {
	// Set transparent
//}
