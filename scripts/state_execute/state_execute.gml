///@func state_execute()
///@description


if(script_exists(state)) {
	if state_new {
		var exit_script = ds_map_find_value(state_exit_map, state_last);
		if not is_undefined(exit_script) and script_exists(exit_script)
			script_execute(exit_script);

		var entry_script = ds_map_find_value(state_entry_map, state);
		if not is_undefined(entry_script) and script_exists(entry_script)
			script_execute(entry_script);
	}

	script_execute(state);
} else {
	state_switch(ds_map_find_first(state_map));
}
