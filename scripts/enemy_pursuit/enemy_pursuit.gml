///@func enemy_pursuit()
///@description enemy pursues player if visible, last player pos if not


var player_dist = point_distance(x, y, global.player_inst.x, global.player_inst.y);

path_speed = move_speed*delta_time/1000000;

if check_player_visible() {
	last_known_x = global.player_inst.x;
	last_known_y = global.player_inst.y;
}

if facing_direction == 0 {
	gun.point_dir = 0;
} else if facing_direction == 180 {
	gun.point_dir = 180;
}

if player_dist < (attack_radius-50) and check_player_visible() {
	face_player();
	path_end();
	state_switch("Attack");
	
} else if path_position == 1 {
	if point_distance(x, y, last_known_x, last_known_y) < .1 {
		face_player();
		path_end();
		if pursuit_path != noone
			path_delete(pursuit_path);
		pursuit_path = path_add();
	
		if mp_grid_path(global.path_grid, pursuit_path, x, y, last_known_x, last_known_y, 1)
			path_start(pursuit_path, path_speed, path_action_stop, false);
		
	} else {
		path_end();
		if pursuit_path != noone {
			path_delete(pursuit_path);
			pursuit_path = noone;	
		}
		state_switch("ResetPause");
	}
}
