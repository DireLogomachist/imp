///@func player_vent()
///@description


// check for directional input
// switch to vent dash state
for (var i = 0; i < array_length_1d(movement_inputs); i++) {
	var this_key = movement_inputs[i];
	if keyboard_check_pressed(this_key) {
		var target_vent = noone;
		if (i == 0) target_vent = current_vent.node_right;
		else if (i == 1) target_vent = current_vent.node_up;
		else if (i == 2) target_vent = current_vent.node_left;
		else if (i == 3) target_vent = current_vent.node_down;
		
		if target_vent != noone {
			facing_direction = 90*i;
			current_vent = target_vent;
			state_switch("VentDash");
		}
	}
}

// check for exit input
// set position and switch to default state
if keyboard_check_pressed(global.action_input) and (current_vent.object_index == vent_entrance or object_is_ancestor(current_vent.object_index, vent_entrance)) {
	sprite_index = spr_imp;
	x =  x + lengthdir_x(25, current_vent.exit_dir);
	y =  y + lengthdir_y(25, current_vent.exit_dir);
	facing_direction = current_vent.exit_dir;
	
	shader_set(duct_shader);
	shader_set_uniform_f(global.duct_alpha, 0.0)
	shader_reset();
	
	for(var i=0; i<array_length_1d(global.rm_manager.dark_layers); i++){
		layer_shader(layer_get_id(global.rm_manager.dark_layers[i]), -1);
	}
	
	audio_play_sound(au_dash, 0, false);
	audio_stop_sound(au_vent_background);
	audio_play_sound(au_room_background, 0, true);
	state_switch("Default");
}
