///@func player_vent_dash
///@description


// using facing_direction and current_vent
// rapidly move to current_vent
var goal_x = current_vent.x + current_vent.offset_x;
var goal_y = current_vent.y + current_vent.offset_y;
var step = dash_speed*delta_time/1000000;
x = x + lengthdir_x(step, facing_direction);
y = y + lengthdir_y(step, facing_direction);

if point_distance(x, y, goal_x, goal_y) < 25 {
	x = goal_x;
	y = goal_y;
	state_switch("Vent");
}
