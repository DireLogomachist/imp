/// @func convert_patrol_path(patrol_str)
/// @description translates a string into a gml grid path

var path_text = argument0;

spawn_origin_x = x;
spawn_origin_y = y;
spawn_direction = facing_direction;
patrol_mode = "off";

if path_text != "" {
	var args = split_string(path_text);
	patrol_path = path_add();
	
	var px = spawn_origin_x;
	var py = spawn_origin_y;
	path_add_point(patrol_path, px, py, 100);
	
	for(var i=1; i < array_length_1d(args); i++) {
		var step_dir = string_char_at(args[i], 1)
		var step_dist = real(string_copy(args[i],2,string_length(args[i])-1))
		
		if (step_dir == "u") py = py - step_dist*32;
		if (step_dir == "d") py = py + step_dist*32;
		if (step_dir == "l") px = px - step_dist*32;
		if (step_dir == "r") px = px + step_dist*32;
		
		path_add_point(patrol_path, px, py, 100);
	}
	
	if (args[0] == "r") {
		patrol_mode = "reverse";
		var pathcopy = path_duplicate(patrol_path);
		path_reverse(pathcopy);
		path_append(patrol_path, pathcopy);
		path_set_closed(patrol_path, false);
	} else if (args[0] == "l") {
		patrol_mode = "loop";
		path_set_closed(patrol_path, true);
	}
	path_start(patrol_path, patrol_speed*delta_time/1000000, path_action_restart, false);
	state_switch("Patrol");
}
