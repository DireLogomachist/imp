///@func enemy_attack()
///@description attack player if within attack radius, if not switch to pursuit


if ds_map_find_value(state_keys, state_next) == "Possessed"
	return;

var player_dist = point_distance(x, y, global.player_inst.x, global.player_inst.y);

if check_in_bounds(global.player_inst.x, global.player_inst.y) {
	last_known_x = global.player_inst.x;
	last_known_y = global.player_inst.y;
}

scan()
if player_dist < attack_radius and check_player_visible() {
	face_player();
	gun.point_dir = point_direction(x, y, last_known_x, last_known_y);
	if fire_available {
		guard_fire();
		fire_available = false;
		alarm[0] = fire_countdown;
	}
} else if pursuit_disabled == true {
	state_switch("Idle");
	gun.visible = false;
} else {
	state_switch("Pursuit");
}
