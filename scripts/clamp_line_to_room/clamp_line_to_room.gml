///@func clamp_line_to_room(x1, y1, x2, y2)
///@description

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;

var ratio = (y2-y1)/(x2-x1);

if x2 < 0 or x2 > room_width {
	x2 = clamp(x2, 5, room_width-5);
	y2 = (x2-x1)*ratio + y1;
}

if y2 < 0 or y2 > room_height {
	y2 = clamp(y2, 5, room_height-5);
	x2 = (y2-y1)/ratio + x1;
}

return array(x2, y2);
