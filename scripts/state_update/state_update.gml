///@func state_update()
///@description


if(state_next != state) {
	state_last=state;
	state=state_next;
	state_timer=0;
	state_start_time = get_timer();
	state_new=true;
} else {
	// Counts frames
	state_timer = state_timer + 1;
	state_new=false;
}
