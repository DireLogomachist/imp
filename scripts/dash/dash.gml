///@func dash(dir, spd)
// DEPRECRATED - DO NOT CALL

var dir = argument0;
var spd = argument1;

var move_speed_this_frame = spd*delta_time/1000000;
var move_x = lengthdir_x(1, dir);
var move_y = lengthdir_y(1, dir);

var move_vec = check_collision(move_x, move_y, move_speed_this_frame);
if (move_vec[0] != 0 or move_vec[1] != 0) {
	move(move_speed_this_frame,  point_direction(0,0,move_vec[0],move_vec[1]));
}

with dash_hitbox {
	var collided = instance_place(x, y, enemy);
	if collided != noone {
		with other {
			possess(collided);
		}
	}
}
