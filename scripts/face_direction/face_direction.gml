///@func face_direction()
///@description change sprite image index to match facing direction


if facing_direction >= 0 and facing_direction <  90 {
	image_index = 0;
} else if facing_direction >= 90 and facing_direction <  180 {
	image_index = 1;
} else if facing_direction >= 180 and facing_direction <  270 {
	image_index = 2;
} else if facing_direction >= 270 and facing_direction <  360 {
	image_index = 3;
}
