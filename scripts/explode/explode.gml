///@func explode()
///@description kills possessed enemy and releases the player


audio_play_sound(au_body_explosion, 0, false)

x = possessed_object.x;
y = possessed_object.y;
possessed_object.player_controlled = false;
possessed_object.solid = false;
with possessed_object {
	instance_destroy(self.gun);
	instance_destroy(self.obj_shadow);
	instance_destroy();
}

possessed_object = noone;

solid = true;
visible = true;

var splat = instance_create_layer(x, y, "Splatter", splatter);
splat.image_xscale = sign(random_range(-1,1));

var fleck1 = instance_create_layer(x, y, "Splatter", blood_fleck);
var fleck2 = instance_create_layer(x, y, "Splatter", blood_fleck);
var fleck3 = instance_create_layer(x, y, "Splatter", blood_fleck);
var fleck4 = instance_create_layer(x, y, "Splatter", blood_fleck);
fleck2.dir += 90;
fleck3.dir += 180;
fleck4.dir += 270;
