///@func state_create_entry(State, EntryScript)
///@description


var key = ds_map_find_value(state_map, argument[0]);

if not is_undefined(key)
	ds_map_replace(state_entry_map, key, argument[1]);
