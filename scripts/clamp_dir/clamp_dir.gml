///@func clamp_dir(dir_var)
///@description


var dir_var = argument0;

if (dir_var > 360)
	dir_var -= 360;

if (dir_var < 0)
	dir_var += 360;
	
return dir_var
