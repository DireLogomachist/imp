
///@func check_collision(dx, dy, spd)

var dx = argument0
var dy = argument1
var spd = argument2

// Check ahead for collision
if tile_meeting(x+dx*spd, y, global.rm_manager.walls) {
	// Snuggle up to wall
	while (!tile_meeting(x+sign(dx), y, global.rm_manager.walls)) {
		x += sign(dx);
	}
	dx = 0;
}

if tile_meeting(x, y+dy*spd, global.rm_manager.walls) {
	while (!tile_meeting(x, y+sign(dy), global.rm_manager.walls)) {
		y += sign(dy);
	}
	dy = 0;
}

var vector;
vector[0] = dx;
vector[1] = dy;
return vector
