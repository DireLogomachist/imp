///@func state_create_exit(State, ExitScript)
///@description


var key = ds_map_find_value(state_map, argument[0]);

if not is_undefined(key)
	ds_map_replace(state_exit_map, key, argument[1]);
