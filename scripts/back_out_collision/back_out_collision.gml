///@func back_out_collision(px, py, ndir)
///@description


var px = argument0;
var py = argument1;
var ndir = argument2;

if (ndir[0] == 0 and ndir[1] == 0)
	return;

while place_meeting(px, py, static_object) {
	px = px - ndir[0];
	py = py - ndir[1];
}
