///@func face_player();
///@description orients the object to face the player


var player_inst = instance_find(player, 0);
var player_dir = point_direction(x, y, player_inst.x, player_inst.y);

facing_direction = 45*round(player_dir/45);
