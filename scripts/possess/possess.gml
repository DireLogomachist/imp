///@func possess(enemy_obj)
///@description player takes control of enemy


var enemy_obj = argument0;
possessed_object = enemy_obj;

with possessed_object {
	state_switch("Possessed");
	alarm[2] = 0.4*room_speed;	// Delayed movement enable
	alarm[1] = -1; // Prevent delayed state switch

	path_end();
	if pursuit_path != noone {
		path_delete(pursuit_path);
		pursuit_path = noone;
	}
	gun.visible = false;
	hopping = false;
}

solid = false;
visible = false;
obj_shadow.visible = false;
x = possessed_object.x;
y = possessed_object.y;

instance_create_layer(x, y-5, "Splatter", possession_circle);
part_emitter_region(pos_sys, pos_em, x-32, x+32, y-16, y+16, ps_shape_ellipse, ps_distr_linear);
part_emitter_burst(pos_sys, pos_em, pos_part, 10);
