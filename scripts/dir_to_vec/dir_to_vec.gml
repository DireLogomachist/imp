///@func dir_to_vec(dir)
///@description converts a direction value to a vec2


var dir = argument0;
return array(cos(dir*pi/180), sin(dir*pi/180));
