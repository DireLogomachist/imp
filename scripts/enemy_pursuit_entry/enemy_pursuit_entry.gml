///@func enemy_pursuit_entry()
///@description


path_end();
if pursuit_path != noone
	path_delete(pursuit_path);

pursuit_path = path_add();
if mp_grid_path(global.path_grid, pursuit_path, x, y, last_known_x, last_known_y, 1) {
	path_start(pursuit_path, move_speed*delta_time/1000000, path_action_stop, false);
} else {
	show_debug_message("Out of bounds pursuit");
}

hopping = true;

gun.visible = true;
gun.point_dir = 0;
