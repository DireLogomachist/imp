///@func raycast_check_walls(x1, y1, x2, y2)
///@description Return true if there is a wall between two given points

// Taking two coordinates, sample points between them and check each for a tile collision

var x1 = argument0;
var y1 = argument1;
var x2 = argument2;
var y2 = argument3;

var hit_pos;
hit_pos[0] = x2;
hit_pos[1] = y2;
hit_pos[2] = false;

var dist = point_distance(x1, y1, x2, y2);
var n = dist/3;

for(var i=1; i<n; i++;) {
	var nx = x1 + i*(x2-x1)/n;
	var ny = y1 + i*(y2-y1)/n;
	
	if tilemap_get_at_pixel(global.rm_manager.walls, nx, ny) {
		hit_pos[0] = nx;
		hit_pos[1] = ny;
		hit_pos[2] = true;
		break;
	}
}

return hit_pos;
