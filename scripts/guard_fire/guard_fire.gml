///@func guard_fire()
///@description

// Pick a random direction within a radius of the player dir and check for a collision
// Draw a line from the enemy gun to the collision end, whether player or a wall

// Pick dir
// Check for player collision - using collision_line_point
// If so call raycast_check_walls with player pos
// If not call raycast_check_walls with dist 2000


var player_inst = instance_find(player, 0);
var player_dir = point_direction(x, y, player_inst.x, player_inst.y);
var radius = 10;

var dir_high = player_dir + radius/2;
var dir_low = player_dir - radius/2;

if (dir_high < dir_low) {
	var tmp = dir_high;
	dir_high = dir_low;
	dir_low = tmp;
}

var fire_dir = 0;
if dir_low < 0 {
	dir_low -= 90; dir_high -= 90;
	fire_dir = random_range(dir_low, dir_high);
	fire_dir = clamp_dir(fire_dir + 90)
} else if dir_high > 360 {
	dir_low += 90; dir_high += 90;
	fire_dir = random_range(dir_low, dir_high);
	fire_dir = clamp_dir(fire_dir - 90)
} else {
	fire_dir = random_range(dir_low, dir_high);
}

var hit_check = collision_line_point(x, y, x + lengthdir_x(2000, fire_dir), y + lengthdir_y(2000, fire_dir), player, false, true);
var hit_pos; hit_pos[0] = hit_check[1]; hit_pos[1] = hit_check[2];

if hit_check[0] != noone {
	hit_pos = raycast_check_walls(x, y, hit_pos[0], hit_pos[1]);
	if hit_pos[2] == false
		with player_inst { player_death(); }
} else {
	hit_pos = raycast_check_walls(x, y, x + lengthdir_x(2000, fire_dir), y + lengthdir_y(2000, fire_dir));
}

var shot = instance_create_layer(0, 0, "VFX", gunshot);
shot.x1 = gun.muzzle_x;
shot.y1 = gun.muzzle_y;
shot.x2 = hit_pos[0];
shot.y2 = hit_pos[1];

var hit = instance_create_layer(hit_pos[0], hit_pos[1], "VFX", bullet_hit);
hit.image_angle = clamp_dir(fire_dir+90);

if irandom(1) == 0 {
	audio_play_sound(au_gunshot_1, 0, false);
} else {
	audio_play_sound(au_gunshot_2, 0, false);
}
