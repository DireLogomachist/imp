/// @func split_string(str)
/// @description splits a string into an array on spaces


var text = argument0;
var split;
var split_index = 0;
var wip_substring = "";

for (var i = 1; i < string_length(text)+1; i++) {
	var c = string_copy(text, i, 1);
	if (c == " ") {
		split[split_index] = wip_substring;
		split_index++;
		wip_substring = "";
	} else {
		wip_substring = wip_substring + c;
		split[split_index] = wip_substring;
	}
}

return split;
