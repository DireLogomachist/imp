///@func state_cleanup()
///@description


ds_map_destroy(state_map);
ds_map_destroy(state_keys);
ds_stack_destroy(state_stack);
ds_map_destroy(state_entry_map);
ds_map_destroy(state_exit_map);
