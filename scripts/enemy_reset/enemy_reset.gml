///@func enemy_reset()
///@description enemy returns to default position and state


facing_direction = direction;

if path_position == 1 {
	if patrol_mode != "off" {
		x = spawn_origin_x;
		y = spawn_origin_y;
		path_start(patrol_path, move_speed*delta_time/1000000, path_action_restart, false);
		path_position = patrol_paused_progress;
		state_switch("Patrol");
	} else {
		facing_direction = spawn_direction;
		state_switch("Idle");
	}
}

scan();
