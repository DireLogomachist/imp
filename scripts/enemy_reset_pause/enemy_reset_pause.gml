///@func enemy_reset_pause()
///@description pause before reset

hopping = false;
if state_timer > reset_pause_duration {
	state_switch("Reset");
}
