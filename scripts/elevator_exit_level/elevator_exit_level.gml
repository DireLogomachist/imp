/// @func elevator_exit_level()
/// @description exits the level via the elevator


// Pause player movement
with global.player_inst {
	player_controlled = false;
	hopping = false;
}

// Close elevator doors
doors.image_speed = -doors.resume_speed;
audio_play_sound(au_elevator_close, 0, false);

// Fade out level
alarm[0] = fade_delay;

// Load next level
alarm[1] = exit_delay;
