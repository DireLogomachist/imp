///@func check_player_visible()
///@description returns whether the player is visible to the calling object


var player_inst = instance_find(player, 0);
var ray_check = raycast_check_walls(x, y, player_inst.x, player_inst.y);
var found_wall = ray_check[2];
if found_wall == true {
	return false;
}

var player_dir = point_direction(x, y, player_inst.x, player_inst.y);
var player_pos = collision_line_point(x, y, x + lengthdir_x(800, player_dir), y + lengthdir_y(800, player_dir), player, false, true);
if player_pos == noone {
	return false;
}

var inner_wall_pos = collision_line_point(x, y, player_pos[1], player_pos[2], inner_wall, false, true);
if inner_wall_pos[0] != noone and player_pos != noone {
	hitwall = array(inner_wall_pos[1], inner_wall_pos[2]);

	var wall_dist = point_distance(x, y, inner_wall_pos[1], inner_wall_pos[2]);
	var player_dist = point_distance(x, y, player_pos[1], player_pos[2]);

	if wall_dist < player_dist or wall_dist == player_dist {
		found_wall = true;
	}
}

if (player_inst.state_name == "Default" or player_inst.state_name == "Dash" or player_inst.state_name == "Dead") and !found_wall
	return true
else
	return false
