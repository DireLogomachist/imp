///@func base_movement()
///@description base movement for both player and possessed enemies (movables)

var move_speed_this_frame = move_speed*delta_time/1000000;
var move_xinput = 0;
var move_yinput = 0;

for (var i = 0; i < array_length_1d(movement_inputs); i++) {
	var this_key = movement_inputs[i];
	if keyboard_check(this_key) {
	    var this_angle = i*90;
	    move_xinput += lengthdir_x(1, this_angle);
	    move_yinput += lengthdir_y(1, this_angle);
	}
}

var moving = (point_distance(0,0,move_xinput,move_yinput) > 0);
if moving {
	hopping = true;
	var move_vec = check_collision(move_xinput, move_yinput, move_speed_this_frame);
	if (move_vec[0] != 0 or move_vec[1] != 0) {
		move(move_speed_this_frame,  point_direction(0,0,move_vec[0],move_vec[1]));
	}
} else {
	hopping = false;	
}
