///@func scan()
///@description scans surroundings for player

// Raycasts a number of lines within a given vision radius


var check_visible = check_player_visible();

for(var i=0; i<scan_num_rays; i++;) {
	var ray_dir = facing_direction - scan_radius/2 + scan_radius/scan_num_rays/2 + i*scan_radius/scan_num_rays;
	if ray_dir < 0 or ray_dir > 360
		ray_dir -= sign(ray_dir)*360;
	
	var scan_x = x + lengthdir_x(scan_dist, ray_dir);
	var scan_y = y + lengthdir_y(scan_dist, ray_dir);
	
	raycasts[i,0] = x;
	raycasts[i,1] = y;
	raycasts[i,2] = scan_x;
	raycasts[i,3] = scan_y;
	raycasts[i,4] = false;
	
	var hit = collision_line_point(x, y, scan_x, scan_y, player, false, true);
	
	if hit[0] != noone and check_visible and (hit[0].state_name != "Possessing") {
		if state_name == "Idle" or state_name == "Patrol" or state_name == "Reset" {
			state_switch("Alert");
			instance_create_depth(x, y-40, "Entities", ui_alert);
			alarm[1] = alert_wait;
		}
		raycasts[i,4] = true;
	}
}
