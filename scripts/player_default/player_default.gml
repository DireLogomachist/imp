///@description default player state


// player movement and more
if player_controlled {
	base_movement();
}

player_action_check();

if keyboard_check_pressed(global.dash_input) and alarm[1] < 0 {
	state_switch("Dash");
	alarm[1] = action_cooldown;
}
