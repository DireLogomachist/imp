///@description


var door = inst_54D2358A;
with door {
	alarm[0] = 1*room_speed;
}
audio_play_sound(au_door_open, 0, false);

base_gbutton_action();
