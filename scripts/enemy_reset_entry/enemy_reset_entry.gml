///@func enemy_reset()
///@description enemy returns to default position and state


if reset_path != noone
	path_delete(reset_path);

gun.visible = false;

hopping = true;
reset_path = path_add();

if patrol_mode != "off" {
	if mp_grid_path(global.path_grid, reset_path, x, y, patrol_paused_x, patrol_paused_y, 1)
		path_start(reset_path, move_speed*delta_time/1000000, path_action_stop, false);
} else {
	if mp_grid_path(global.path_grid, reset_path, x, y, spawn_origin_x, spawn_origin_y, 1) {
		path_start(reset_path, move_speed*delta_time/1000000, path_action_stop, false);
	} else {
		show_debug_message("IMPOSSIBLE PATH: " + string(spawn_origin_x) + "/" + string(spawn_origin_y));
	}
}
