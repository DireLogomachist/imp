///@func enter_vent(vent)
///@description


var vent = argument0;
var player_inst = instance_find(player, 0);

with player_inst {
	state_switch("Vent");
	self.x = vent.x + vent.offset_x;
	self.y = vent.y + vent.offset_y;
	self.current_vent = vent;
}

shader_set(duct_shader);
shader_set_uniform_f(global.duct_alpha, 1.0);
shader_reset();

for(var i=0; i<array_length_1d(global.rm_manager.dark_layers); i++){
	layer_shader(layer_get_id(global.rm_manager.dark_layers[i]), darken_all_shader);
}

audio_play_sound(au_dash, 0, false);
audio_stop_sound(au_room_background);
audio_play_sound(au_vent_background, 0, true);
